// Just a small node script to copy the gamebook.js file to the cordova demo cross-platform
const fs = require('fs');
fs.mkdirSync('demo/cordova/www/js/');
fs.copyFile('dist/gamebook.js', 'demo/cordova/www/js/gamebook.js', (err) => {
	if (err) throw err;
	console.log("Copied distribution to demo.");
})