const path = require('path');

module.exports = {
    entry: ['./src/index.ts'],
	devtool: 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.(ts|js)$/,
				use: [{
					loader: 'expose-loader',
					options: 'GB',
				},
				{
					loader: 'babel-loader',
				}]
			},{
				test: /\.scss$/,
				use: ['style-loader', 'css-loader', 'sass-loader'],
			},
		],
	},
	resolve: {
		extensions: ['.ts','.js'],
	},
    output: {
        filename: 'gamebook.js',
        path: path.resolve(__dirname, 'dist'),
    },
};