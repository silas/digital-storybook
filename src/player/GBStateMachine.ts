import {GBLang} from "../language/GBLang";
import {GBNode} from "../common/Node/GBNode";
import {GBSave} from "../common/GBSave";
import {GBNodeContainer} from "../common/GBNodeContainer";

export class GBStateMachine {
    states: { [id: number]: GBNode };
    private currentState: GBNode;

    constructor(save: GBSave | undefined = undefined) {
        this.states = {};
        if (save !== undefined) {
            this.Load(save);
            this.currentState = this.states[0];
        } else {
            this.currentState = GBNodeContainer.constructNode("Decision");
            this.currentState.SetAttribute("room", GBLang.tl("unloaded", true));
        }
    }

    /**
     * Loads a gamebook
     * @param save Save to load
     */
    public Load(save: GBSave) {
        save.n.forEach(node => {
            let n = GBNodeContainer.constructNode(node.t);
            n.Deserialize(node.a);
            let id = n.GetAttribute("id");
            this.states[id] = n;
        });
    }

    /**
     * Gets the current state
     */
    public get state(): GBNode {
        return this.currentState;
    }

    /**
     * Sets the current state by id
     * @param id ID of state
     */
    public SetState(id: number) {
        if (this.states[id] !== undefined) {
            this.currentState = this.states[id];
        }
    }

    /**
     * Transition through a given output by id
     * @param output Output ID
     */
    public Transition(output: number) {
        let id = <number>this.state.ListAttributeGet("outputTargets", output);
        this.currentState = this.states[id];
    }

    /**
     * Fetch a state by ID
     * @param id State ID
     * @returns Requested node
     */
    public FetchState(id: number): GBNode {
        return this.states[id];
    }
}
