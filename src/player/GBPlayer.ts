import {GBStateMachine} from "./GBStateMachine";
import {GBRoomNode} from "../common/Node/GBRoomNode";
import {GBToolNode} from "../common/Node/GBToolNode";
import {GBMediaNode} from "../common/Node/GBMediaNode";
import {GBEndNode} from "../common/Node/GBEndNode";
import {GBSave} from "../common/GBSave";
import {GBRender} from "../common/GBRender";
import {SecureEvaluate} from "../common/GBMath";
import {GBLang} from "../language/GBLang";
import {GBMenu} from "../common/Menu/GBMenu";
import {GBMenubar} from "../common/Menu/GBMenubar";

interface SaveState {
    hash: number;
    vars: { [key: string]: any };
    state: number;
    pages: string[];
}

export class GBPlayer {
    private root: HTMLDivElement;
    private playerContainer: HTMLDivElement;
    private pageContainer: HTMLDivElement;
    private pageContent: HTMLDivElement;
    private controlBar: HTMLDivElement;
    private pages: string[] = [];
    private currentPage: HTMLDivElement | null = null;
    private pageNumber: number = 0;
    private callback: (rating: any) => void;
    private _menu: GBMenubar;
    private menuContainer: HTMLDivElement;
    private _fontSize: number = 24;
    private nodeRenderedCallback: any = null;

    public stateMachine: GBStateMachine;
    public variableRegister: { [key: string]: any } = {};

    constructor(
        container: HTMLElement,
        callback: (rating: any) => void,
        language: string = "en"
    ) {
        GBLang.SetLanguage(language);
        this.root = document.createElement("div");
        this.root.setAttribute("class", "GBPlayer");

        // Menu
        this.menuContainer = document.createElement("div");
        this.menuContainer.setAttribute("class", "GBMenuContainer");
        this._menu = new GBMenubar(this.menuContainer, "GBMenu");
        this.menu.AddMenuItem(GBLang.tl("back", true) as string, () => {
            this._menu.Close();
            this.root.removeChild(this.menuContainer);
            this.root.appendChild(this.playerContainer);
        });
        let fontMenu = this.menu.AddSubMenu(GBLang.tl("font", true) as string);
        fontMenu.AddMenuItem(GBLang.tl("incSize", true) as string, () => {
            this.fontSize += 2;
        });
        fontMenu.AddMenuItem(GBLang.tl("decSize", true) as string, () => {
            this.fontSize -= 2;
        });

        // Controlbar
        this.controlBar = document.createElement("div");
        this.controlBar.setAttribute("class", "GBControlBar");

        // Jump to page button
        let button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "^");
        button.setAttribute("aria-label", GBLang.tl("jumpPage", true) as string);
        button.addEventListener("click", () => {
            let page = Number(
                prompt(GBLang.tl("pagePrompt", true) as string + (this.pages.length + 1))
            );
            if (page != NaN && page != 0) {
                this.GoToPage(page);
            }
        });
        this.controlBar.appendChild(button);

        // Previous page button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "<");
        button.setAttribute("aria-label", GBLang.tl("previousPage", true) as string);
        button.addEventListener("click", () => {
            if (this.pageNumber > 1) {
                this.GoToPage(this.pageNumber - 1);
            }
        });
        this.controlBar.appendChild(button);

        // Menu button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "...");
        button.setAttribute("aria-label", GBLang.tl("menu", true) as string);
        button.addEventListener("click", () => {
            this._menu.Open();
            this.root.removeChild(this.playerContainer);
            this.root.appendChild(this.menuContainer);
        });
        this.controlBar.appendChild(button);

        // Next page button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", ">");
        button.setAttribute("aria-label", GBLang.tl("nextPage", true) as string);
        button.addEventListener("click", () => {
            if (this.pageNumber <= this.pages.length) {
                this.GoToPage(this.pageNumber + 1);
            }
        });
        this.controlBar.appendChild(button);

        // Current page button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", ">>");
        button.setAttribute("aria-label", GBLang.tl("currentPage", true) as string);
        button.addEventListener("click", () => {
            this.GoToCurrentPage();
        });
        this.controlBar.appendChild(button);

        // PageContainer and PageContent
        this.pageContainer = document.createElement("div");
        this.pageContainer.setAttribute("class", "GBPageContainer");
        this.pageContent = document.createElement("div");
        this.pageContainer.appendChild(this.pageContent);
        this.playerContainer = document.createElement("div");
        this.playerContainer.appendChild(this.pageContainer);
        this.playerContainer.appendChild(this.controlBar);
        this.root.appendChild(this.playerContainer);
        container.appendChild(this.root);

        this.callback = callback;
        this.stateMachine = new GBStateMachine();
        this.DoState();
    }

    /**
     * Disable all input elements in a given element.
     * @param element Element in which all inputs should be disabled
     */
    private DisableInputs(element: HTMLElement) {
        element.querySelectorAll("input").forEach(input => {
            input.disabled = true;
            if (input.type == "checkbox") {
                input.setAttribute("checked", input.checked.toString());
            } else {
                input.setAttribute("value", input.value);
            }
        });
    }

    /**
     * Execute the internal logic of the current state.
     */
    public DoState() {

        let state = this.stateMachine.state;
        if (state instanceof GBRoomNode) {
            let container = document.createElement("div");
            state.Render(
                output => {
                    this.Select(output);
                },
                this.variableRegister,
                container
            );

            this.currentPage = container;
            this.pageContent.appendChild(this.currentPage);
            this.pageContainer.scrollTop = 0;
            this.pageNumber++;

            if (typeof this.nodeRenderedCallback === "function") {
                this.nodeRenderedCallback(container, state);
            }
        } else if (state instanceof GBToolNode) {
            this.Select(state.Execute(this.variableRegister));
        } else if (state instanceof GBEndNode) {
            this.callback(
                SecureEvaluate(state.GetAttribute("rating"), this.variableRegister)
            );
        }
    }

    public setNodeRenderedCallback(callback) {
        this.nodeRenderedCallback = callback;
    }

    /**
     * Select an output of the current state.
     * @param output ID of selected output
     */
    private Select(output: number) {
        if (this.currentPage !== null) {
            this.DisableInputs(this.pageContent);
            this.pages.push(this.pageContent.innerHTML);
        }
        this.pageContent.innerHTML = "";
        this.currentPage = null;
        this.stateMachine.Transition(output);
        this.DoState();
    }

    /**
     * Load a gamebook from a GBSave structure.
     * @param save GBSave structure to load
     */
    public LoadGamebook(save: GBSave) {
        this.pageContent.textContent = "";
        this.pages = [];
        this.variableRegister = {};
        this.currentPage = null;
        this.pageNumber = 0;
        this.stateMachine = new GBStateMachine(save);
        let media: { [tag: string]: GBMediaNode } = {};
        save.i.forEach(imageString => {
            let image = new GBMediaNode();
            image.Deserialize(imageString);
            media[image.GetAttribute("tag")] = image;
        });
        let startNode = this.stateMachine.FetchState(0);
        SecureEvaluate(startNode.GetAttribute("init"), this.variableRegister);
        GBRender.GetRenderer().SetPrefix(startNode.GetAttribute("prefix"));
        GBRender.GetRenderer().SetMedia(media);
        this.DoState();
    }

    /**
     * Deserialize JSON and then load it.
     * @param gamebookJson JSON string
     */
    public LoadGamebookFromString(gamebookJson: string) {
        let save = JSON.parse(gamebookJson) as GBSave;
        this.LoadGamebook(save);
    }

    /**
     * Jump to the last page.
     */
    public GoToCurrentPage() {
        this.pageNumber = this.pages.length + 1;
        if (this.currentPage !== null) {
            this.pageContent.innerHTML = "";
            this.pageContent.appendChild(this.currentPage);
        }
    }

    /**
     * Jump to a given page.
     * @param page Page to jump to
     */
    public GoToPage(page: number) {
        if (page <= this.pages.length && page > 0) {
            this.pageNumber = page;
            this.pageContent.innerHTML = this.pages[page - 1];
        } else {
            this.GoToCurrentPage();
        }
    }

    /**
     * Serialize the current savestate to a JSON string.
     * @returns Savestate JSON
     */
    public SaveState(): string {
        let save: SaveState = {
            hash: this.HashString(
                JSON.stringify(this.stateMachine.states) +
                JSON.stringify(this.variableRegister) +
                this.stateMachine.state.GetAttribute("id")
            ),
            vars: this.variableRegister,
            state: this.stateMachine.state.GetAttribute("id"),
            pages: this.pages
        };
        return JSON.stringify(save);
    }

    /**
     * Deserializes a JSON string and loads the savestate.
     * @param state Savestate JSON
     */
    public LoadState(state: string) {
        let save: SaveState = JSON.parse(state);
        if (this.CheckCompatibility(save)) {
            this.pageContent.innerHTML = "";
            this.stateMachine.SetState(save.state);
            this.pages = save.pages;
            this.variableRegister = save.vars;
            this.pageNumber = this.pages.length;
            this.DoState();
        } else {
            this.pageContent.appendChild(GBLang.tl("compatible") as Element);
        }
    }

    /**
     * A simple hash function.
     * It is used to make it impossible to cross-load savestates.
     * @param text String to hash
     */
    private HashString(text: string): number {
        let hash = 0,
            i,
            chr;
        for (i = 0; i < text.length; i++) {
            chr = text.charCodeAt(i);
            hash = (hash << 5) - hash + chr;
            hash |= 0;
        }
        return hash;
    }

    /**
     * Check the compatibility of a savestate with the current gamebook.
     * @param save SaveState structure to check compatibility with
     * @returns true if the savestate is compatible, false if not.
     */
    private CheckCompatibility(save: SaveState): boolean {
        return (
            this.HashString(
                JSON.stringify(this.stateMachine.states) +
                JSON.stringify(save.vars) +
                save.state
            ) === save.hash
        );
    }

    getCurrentPageNumber(){
        return this.pageNumber;
    }

    /// Accessors
    get menu(): GBMenu {
        return this._menu.menu;
    }

    get fontSize(): number {
        return this._fontSize;
    }

    set fontSize(size: number) {
        if (size > 0) {
            this.root.style["fontSize"] = size + "px";
            this._fontSize = size;
        }
    }
}
