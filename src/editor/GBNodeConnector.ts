import {GBNodeElement} from "./GBNodeElement";

export class GBNodeConnector {
    private readonly _node: GBNodeElement;
    public readonly input: boolean;
    private _connectorElement: SVGGElement;
    private _titleElement: SVGTextElement;
    private _connectedTo: GBNodeConnector[] = [];
    private _connectionPath: SVGPathElement | null = null;

    private _title: string = "";
    private _index: number = 0;

    constructor(node: GBNodeElement, input: boolean = false, title: string = "") {
        this._node = node;
        this.input = input;

        this._connectorElement = document.createElementNS(
            GBNodeElement.ns_svg,
            "g"
        );
        this.index = node.outputCount;

        let circle = document.createElementNS(GBNodeElement.ns_svg, "circle");
        circle.setAttribute(
            "cy",
            Math.floor(GBNodeElement.lineheight / 2).toString()
        );
        circle.setAttribute("r", "5");
        circle.style["fill"] = node.node.GetAttribute("color");

        this._titleElement = document.createElementNS(GBNodeElement.ns_svg, "text");
        this.title = title;

        if (input) {
            circle.setAttribute("cx", "0");
            this._titleElement.setAttribute("transform", `translate(10,15)`);
        } else {
            circle.setAttribute("cx", GBNodeElement.nodewidth.toString());
            this._titleElement.setAttribute("text-anchor", "end");
            this._titleElement.setAttribute(
                "transform",
                `translate(${GBNodeElement.nodewidth - 10},15)`
            );
            this._connectionPath = document.createElementNS(
                GBNodeElement.ns_svg,
                "path"
            );
            this._node.editor.connectionContainer.appendChild(this._connectionPath);
            this._connectionPath.addEventListener("contextmenu", e => {
                e.preventDefault();
                e.stopPropagation();
                this.RemoveConnections();
            });
        }

        this._connectorElement.appendChild(circle);
        this._connectorElement.appendChild(this._titleElement);
        this._node.connectorGElement.appendChild(this._connectorElement);
        circle.addEventListener("mousedown", e =>
            this._node.editor.ConnectStart(e, this)
        );
        circle.addEventListener("mouseup", e =>
            this._node.editor.ConnectEnd(e, this)
        );
        circle.addEventListener("contextmenu", e => {
            if (e.button != 2) return;
            e.preventDefault();
            e.stopPropagation();
            this.RemoveConnections();
        });
    }

    CreateConnection(to: GBNodeConnector) {
        if (this.input) {
            this._connectedTo.push(to);
        } else {
            this.RemoveAllPaths();
            this._connectedTo = [to];
            this.path = to;
        }
    }

    RemovePath(to: GBNodeConnector, propagate: boolean = true) {
        if (this.IsConnectedTo(to)) {
            this._connectedTo = this._connectedTo.filter(x => x != to);
            if (!this.input) this.path = null;
            if (propagate) to.RemovePath(this, false);
        }
    }

    RemoveAllPaths() {
        this._connectedTo.forEach(x => this.RemovePath(x, true));
    }

    Remove() {
        this.RemoveAllPaths();
        this.node.connectorGElement.removeChild(this._connectorElement);
    }

    IsConnectedTo(to: GBNodeConnector): boolean {
        return this._connectedTo.indexOf(to) != -1;
    }

    UpdatePaths() {
        if (this.input) {
            this._connectedTo.forEach(connector => {
                connector.UpdatePaths();
            });
        } else {
            if (this._connectedTo[0]) {
                this.path = this._connectedTo[0];
            }
        }
    }

    /* Node modifiers */

    RemoveConnections() {
        if (!this.input) {
            this.node.node.DisconnectOutput(this.index);
        } else {
            this._connectedTo.forEach(x => x.RemoveConnections());
        }
    }

    set path(to: GBNodeConnector | null) {
        if (!this.input && this._connectionPath) {
            if (to) {
                this._connectionPath.setAttribute(
                    "d",
                    this.node.editor.GetPath(this.x, this.y, to.x, to.y)
                );
            } else {
                this._connectionPath.setAttribute("d", "");
            }
        }
    }

    set index(i: number) {
        this._connectorElement.setAttribute(
            "transform",
            `translate(0,${GBNodeElement.lineheight * (i + 1)})`
        );
        this._index = i;
    }

    get index(): number {
        return this._index;
    }

    set title(title: string) {
        this._title = title;
        if (title.length > 16) {
            title = title.substr(0, 16);
            title += "\u{2026}";
        }
        this._titleElement.textContent = title;
    }

    get title(): string {
        return this._title;
    }

    get x(): number {
        if (this.input) return this._node.x + this._node.offsetX;
        else return this._node.x + this._node.offsetX + GBNodeElement.nodewidth;
    }

    get y(): number {
        return (
            this._node.y +
            this._node.offsetY +
            Math.floor(GBNodeElement.lineheight * (this._index + 2.5))
        );
    }

    get node(): GBNodeElement {
        return this._node;
    }

    set color(color: string) {
        this._titleElement.style["fill"] = color;
    }
}
