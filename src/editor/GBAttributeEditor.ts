import {GBLang} from "../language/GBLang";
import {GBNode} from "../common/Node/GBNode";
import {GBEditor} from "./GBEditor";

export class GBAttributeEditor {
    private sidebar: HTMLElement;
    private node: GBNode;
    private editor: HTMLDivElement;
    private nodeEditor: GBEditor;

    constructor(nodeEditor: GBEditor, node: GBNode) {
        this.editor = document.createElement("div");
        this.editor.setAttribute("class", "GBAttributeEditor");
        this.editor.style["backgroundColor"] = node.GetAttribute("color");
        this.editor.style["color"] = node.GetAttribute("textcolor");
        this.node = node;

        this.nodeEditor = nodeEditor;
        if (this.nodeEditor.GetCount() == 0) {
            nodeEditor.sidebarRight.setAttribute("class", "GBSidebar right open");
        }
        let type = this.node.GetAttribute("type");

        this.nodeEditor.GetOpenEditors(type)[this.node.GetAttribute("id")] = this;

        this.sidebar = nodeEditor.sidebarRight;

        node.attrIds.forEach(id => {
            if (!node.GetAttributeProperty(id, "hidden")) {
                this.AddControl(id);
            }
        });

        this.sidebar.appendChild(this.editor);
        this.sidebar.scrollTop = this.sidebar.scrollHeight;

        let exitButton = document.createElement("input");
        exitButton.setAttribute("type", "button");
        exitButton.value = GBLang.tl("close", true) as string;
        exitButton.addEventListener("click", () => this.Close());
        this.editor.appendChild(exitButton);

        if (node.GetAttribute("id") != 0) {
            let deleteButton = document.createElement("input");
            deleteButton.setAttribute("type", "button");
            deleteButton.style["display"] = "block";
            deleteButton.value = GBLang.tl("delete", true) as string;
            let _deleteListener = () => {
                if (type == "editor") nodeEditor.DeleteNode(node.GetAttribute("id"));
                else if (type == "image")
                    nodeEditor.DeleteMedia(node.GetAttribute("id"));
            };
            let _deleteReset = () => {
                deleteButton.value = GBLang.tl("delete", true) as string;
                deleteButton.removeEventListener("click", _deleteListener);
            };
            deleteButton.addEventListener("click", () => {
                deleteButton.value = GBLang.tl("sure", true) as string;
                deleteButton.addEventListener("click", _deleteListener);
                deleteButton.addEventListener("mouseleave", _deleteReset);
            });
            this.editor.appendChild(deleteButton);
        }
    }

    /* Window controls */
    Close() {
        delete this.nodeEditor.GetOpenEditors(this.node.GetAttribute("type"))[this.node.GetAttribute("id")];
        this.sidebar.removeChild(this.editor);
        if (this.nodeEditor.GetCount() == 0) {
            this.sidebar.setAttribute("class", "GBSidebar right");
        }
    }

    // Pushes the editor to the bottom of the list and scrolls down
    Highlight() {
        this.sidebar.appendChild(this.editor);
        this.sidebar.scrollTop = this.sidebar.scrollHeight;
    }

    AddControl(id: string) {
        let row = document.createElement("div");
        let title = document.createElement("label");
        let label = this.node.GetAttributeProperty(id, "label");
        if (label) title.textContent = label;
        else title.textContent = id;
        row.appendChild(title);
        switch (this.node.GetAttributeProperty(id, "type")) {
            case "number": {
                this.AddNumberInput(row, id);
                break;
            }
            case "string": {
                this.AddStringInput(row, id);
                break;
            }
            case "file": {
                this.AddFileInput(row, id);
                break;
            }
            case "boolean": {
                this.AddBooleanInput(row, id);
                break;
            }
            case "select": {
                this.AddSelectInput(row, id);
                break;
            }
            case "list": {
                this.AddListControl(row, id);
                break;
            }
        }
        this.editor.appendChild(row);
    }

    AddNumberInput(row: HTMLDivElement, id: string) {
        let input = document.createElement("input");
        input.setAttribute("type", "number");
        input.value = this.node.GetAttribute(id);
        if (this.node.GetAttributeProperty(id, "readonly"))
            input.setAttribute("readonly", "true");
        row.appendChild(input);
        input.addEventListener("input", () => {
            if (!isNaN(Number(input.value))) this.node.SetAttribute(id, input.value);
        });
    }

    AddFileInput(row: HTMLDivElement, id: string) {
        const input = document.createElement("input");
        input.type = "file";
        input.classList.add("storybook-file-upload");

        const button = document.createElement("button");

        if (this.node.GetAttributeProperty(id, "image")) {
            input.setAttribute("accept", "image/png, image/jpeg, image/svg");
        } else if (this.node.GetAttributeProperty(id, "video")) {
            input.setAttribute("accept", "video/*");
        } else if (this.node.GetAttributeProperty(id, "accept")) {
            input.setAttribute("accept", this.node.GetAttributeProperty(id, "accept"));
        }

        button.innerText = this.node.GetAttributeProperty(id, "name");
        if (this.node.GetAttributeProperty(id, "readonly")) {
            input.setAttribute("readonly", "true");
        }
        const indicatorDone = document.createElement("span");
        input.addEventListener("input", () => {
            indicatorDone.appendChild(GBLang.tl("loading") as Element);
            const filePromise = new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onload = () => resolve(reader.result);
                reader.onerror = error => reject(error);
                reader.readAsDataURL(input.files[0]);
            });
            filePromise.then(r => {
                this.node.SetAttribute(id, r);
                this.node.SetAttributeProperty(id, "name", input.files[0].name);
                button.innerText = input.files[0].name;
                indicatorDone.innerText = "";
            });
        });
        button.addEventListener("click", () => {
            input.click();
        })

        row.appendChild(input);
        row.appendChild(button);
        row.appendChild(indicatorDone);
    }

    AddStringInput(row: HTMLDivElement, id: string) {
        let input: HTMLInputElement | HTMLTextAreaElement;
        if (this.node.GetAttributeProperty(id, "textarea")) {
            input = document.createElement("textarea");
            input.setAttribute("rows", "3");
        } else {
            input = document.createElement("input");
            input.setAttribute("type", "text");
        }
        input.value = this.node.GetAttribute(id);
        if (this.node.GetAttributeProperty(id, "readonly"))
            input.setAttribute("readonly", "true");
        row.appendChild(input);
        input.addEventListener("input", () => {
            let regExp = new RegExp(
                this.node.GetAttributeProperty(id, "regExp") as string
            );
            if (regExp) {
                if (regExp.test(input.value)) {
                    input.setCustomValidity("");
                } else {
                    input.setCustomValidity("Input is not valid!");
                    return;
                }
            }
            this.node.SetAttribute(id, input.value);
        });
        // input.addEventListener("input", () => {
        //     input.dispatchEvent(new Event("change"));
        // })
    }

    AddBooleanInput(row: HTMLDivElement, id: string) {
        let input = document.createElement("input");
        input.setAttribute("type", "checkbox");
        input.checked = this.node.GetAttribute(id);
        if (this.node.GetAttributeProperty(id, "readonly"))
            input.setAttribute("readonly", "true");
        row.appendChild(input);
        input.addEventListener("change", () => {
            this.node.SetAttribute(id, input.checked);
        });
    }

    AddSelectInput(row: HTMLDivElement, id: string) {
        let select = document.createElement("select");
        let choices = this.node.GetAttributeProperty(
            id,
            "selectChoices"
        ) as string[];
        choices.forEach(choice => {
            let option = document.createElement("option");
            option.innerText = choice;
            select.appendChild(option);
        });
        select.value = this.node.GetAttribute(id);
        if (this.node.GetAttributeProperty(id, "readonly")) select.disabled = true;
        row.appendChild(select);
        select.addEventListener("change", () => {
            this.node.SetAttribute(id, select.value);
        });
    }

    AddListControl(
        row: HTMLDivElement,
        id: string
    ) {
        let container = document.createElement("div");
        const list = <any[]>this.node.GetAttribute(id);

        list.forEach((value, index) => {
            const listInputElement = this.AddListInput(id, index, value);
            container.appendChild(listInputElement);
        });

        let addButton = document.createElement("input");
        addButton.setAttribute("type", "button");
        addButton.value = GBLang.tl("addEntry", true) as string;
        addButton.addEventListener("click", () => {
            container.appendChild(this.AddListEntry(id));
        });
        row.appendChild(container);
        if (!this.node.GetAttributeProperty(id, "readonly"))
            row.appendChild(addButton);
    }

    AddListEntry(
        id: string,
    ) {
        const type = this.node.GetAttributeProperty(id, "listType");

        let value: any;
        switch (type) {
            case "number": {
                value = 0;
                break;
            }
            case "string": {
                value = "";
                break;
            }
            case "boolean": {
                value = false;
                break;
            }
            case "select": {
                value = (this.node.GetAttributeProperty(
                    id,
                    "selectChoices"
                ) as string[])[0];
            }
        }
        let index = this.node.ListAttributePush(id, value) - 1;
        return this.AddListInput(id, index, value);
    }

    AddListInput(
        id: string,
        index: number,
        value: any
    ) {
        let row = document.createElement("div");
        row.dataset["index"] = index.toString();

        const type = this.node.GetAttributeProperty(id, "listType");

        let title = document.createElement("label");
        title.textContent = index.toString();
        let input: HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement | HTMLDivElement;
        switch (type) {
            case "number": {
                input = document.createElement("input");
                input.setAttribute("type", "number");
                break;
            }
            case "string": {
                if (this.node.GetAttributeProperty(id, "textarea")) {
                    input = document.createElement("textarea");
                    input.setAttribute("rows", "1");
                } else {
                    input = document.createElement("input");
                    input.setAttribute("type", "text");
                }
                break;
            }
            case "boolean": {
                input = document.createElement("input");
                input.setAttribute("type", "checkbox");
                break;
            }
            case "select": {
                input = document.createElement("select");
                (this.node.GetAttributeProperty(id, "selectChoices") as string[]).forEach(choice => {
                    let option = document.createElement("option");
                    option.innerText = choice;
                    input.appendChild(option);
                });
                break;
            }
            case "fields": {
                const fieldDefinitions = this.node.GetAttributeProperty(id, "fields")
                input = document.createElement("div");

                if (!value) {
                    value = [];
                }
                fieldDefinitions.forEach((definition, i) => {
                    if (!value[i]) {
                        value[i] = "";
                    }
                    const label = document.createElement("label");
                    const titleElem = document.createElement("span");
                    titleElem.innerText = definition.label;
                    label.appendChild(titleElem);

                    let inputElem = null;
                    if (definition.textarea) {
                        inputElem = document.createElement("textarea");
                        inputElem.setAttribute("rows", "3");
                        if (value && value[i]) {
                            inputElem.innerText = value[i];
                        }
                    } else {
                        inputElem = document.createElement("input");
                        if (definition.type) {
                            inputElem.type = definition.type;
                        }
                        if (value && value[i]) {
                            inputElem.value = value[i];
                        }
                        if (definition.checkbox) {
                            inputElem.type = "checkbox";
                            if (value && value[i]) {
                                inputElem.checked = true;
                            }
                        }
                    }
                    label.appendChild(inputElem);
                    input.appendChild(label);

                    inputElem.addEventListener("change", () => {
                        if (definition.checkbox) {
                            value[i] = inputElem.checked;
                        } else {
                            value[i] = inputElem.value;
                        }
                        this.node.ListAttributeSet(
                            id,
                            parseInt(row.dataset["index"]),
                            value
                        );
                    });
                })
                break;
            }
        }
        if (value && type !== "fields") {
            if (type == "boolean") (<HTMLInputElement>input).checked = value;
            else if ("value" in input) {
                input.value = value;
            }
        }

        let removeButton = document.createElement("input");
        removeButton.setAttribute("type", "button");
        removeButton.value = "X";

        removeButton.addEventListener("click", () => {
            const index = parseInt(row.dataset["index"]);
            this.node.ListAttributeRemove(id, index);
            if (row.parentElement) {
                this.UpdateIndices(row.parentElement);
            }
            row.remove();
        });
        if (!(input instanceof HTMLDivElement)) {

            input.addEventListener(type == "string" ? "input" : "change", () => {
                if (type == "string") {
                    let regExp = new RegExp(
                        this.node.GetAttributeProperty(id, "regExp") as string
                    );
                    if (regExp) {
                        // @ts-ignore
                        if (regExp.test(input.value)) {
                            // @ts-ignore
                            input.setCustomValidity("");
                        } else {
                            // @ts-ignore
                            input.setCustomValidity("Input is not valid!");
                            return;
                        }
                    }
                }
                this.node.ListAttributeSet(
                    id,
                    parseInt(row.dataset["index"]),
                    // @ts-ignore
                    type == "boolean" ? (<HTMLInputElement>input).checked : input.value
                );
            });
        }
        row.appendChild(title);
        row.appendChild(input);
        if (this.node.GetAttributeProperty(id, "readonly")) {
            if (type == "select" && input instanceof HTMLSelectElement) input.disabled = true;
            else input.setAttribute("readonly", "true");
        } else {
            row.appendChild(removeButton);
        }
        return row;
    }

    private UpdateIndices(container: HTMLElement) {
        let children = container.children;
        for (let i = 0; i < children.length; i++) {
            (<HTMLElement>children[i]).dataset["index"] = i.toString();
            let label = children[i].querySelector("label");
            if (label) label.textContent = (i).toString();
        }
    }
}
