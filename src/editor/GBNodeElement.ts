import { GBEditor } from "./GBEditor";
import { GBEditorNode } from "../common/Node/GBEditorNode";
import { GBAttributeEditor } from "./GBAttributeEditor";
import {GBNodeConnector} from "./GBNodeConnector";

export class GBNodeElement {
	public static readonly ns_svg = "http://www.w3.org/2000/svg";
	public static readonly nodewidth: number = 180;
	public static readonly lineheight: number = 22;
	private _editor: GBEditor;

	private _nodeElement: SVGGElement;
	private _nodeBody: SVGRectElement;
	private _nodeDivider: SVGLineElement;
	private _nodeTitle: SVGTextElement;
	private _outputContainer: SVGGElement;

	private _node: GBEditorNode;

	private _offsetX: number = 0;
	private _offsetY: number = 0;
	private _width: number = 0;
	private _height: number = 0;

	private _input: GBNodeConnector;
	private _outputs: Array<GBNodeConnector> = [];

	constructor(editor: GBEditor, node: GBEditorNode) {
		this._node = node;

		// Create elements and set constant attributes
		this._nodeElement = document.createElementNS(GBNodeElement.ns_svg, "g");
		this._nodeElement.setAttribute("class", "GBNode");

		this._nodeDivider = document.createElementNS(GBNodeElement.ns_svg, "line");
		this._nodeDivider.setAttribute("x1", "0");
		this._nodeDivider.setAttribute("y1", GBNodeElement.lineheight.toString());
		this._nodeDivider.setAttribute("y2", GBNodeElement.lineheight.toString());

		this._nodeBody = document.createElementNS(GBNodeElement.ns_svg, "rect");
		this._nodeBody.setAttribute("rx", "10px");
		this._nodeBody.setAttribute("ry", "10px");

		this._nodeTitle = document.createElementNS(GBNodeElement.ns_svg, "text");
		this._nodeTitle.setAttribute("transform", "translate(10,15)");

		this._outputContainer = document.createElementNS(GBNodeElement.ns_svg, "g");
		this._outputContainer.setAttribute(
			"transform",
			`translate(0,${GBNodeElement.lineheight})`
		);
		this._input = new GBNodeConnector(this, true, "Input");
		this._input.index = -1;

		// Set properties
		this._editor = editor;
		this.x = node.GetAttribute("x");
		this.y = node.GetAttribute("y");
		this.width = GBNodeElement.nodewidth;
		this.height = GBNodeElement.lineheight * 2;

		// Add to DOM
		this._nodeElement.appendChild(this._nodeBody);
		this._nodeElement.appendChild(this._nodeDivider);
		this._nodeElement.appendChild(this._nodeTitle);
		this._nodeElement.appendChild(this._outputContainer);
		editor.canvas.appendChild(this._nodeElement);

		// Add events
		this._nodeElement.addEventListener("mousedown", e => this.DragStart(e));
		this._nodeElement.addEventListener("contextmenu", e => {
			e.preventDefault();
			e.stopPropagation();

			if (!editor.IsOpen(this.id)) {
				new GBAttributeEditor(this.editor, this._node);
			} else {
				editor.Get(this.id)?.Highlight();
			}
		});
		this._node.On("title", (title: string) => {
			if (title.length > 16) {
				title = title.substr(0, 16);
				title += "\u{2026}";
			}
			this._nodeTitle.textContent = title;
		});
		this._node.On("color", c => (this._nodeBody.style["fill"] = c));
		this._node.On("textcolor", c => {
			this._nodeTitle.style["fill"] = c;
			this._input.color = c;
			this._outputs.forEach(x => (x.color = c));
		});
		this._node.On("outputTargets", x => this.UpdateOutputTargets(x), false);
		this._node.On("outputTitles", x => this.UpdateOutputTitles(x), false);
		this.UpdateVisuals();
	}

	/* Methods */

	public AddOutput(title: string): GBNodeConnector {
		// Prepare DOM elements
		let connector = new GBNodeConnector(this, false, title);

		// Push to array
		this._outputs.push(connector);

		this.height += GBNodeElement.lineheight;
		return connector;
	}

	public UpdatePaths() {
		this._input.UpdatePaths();
		this._outputs.forEach(connector => {
			connector.UpdatePaths();
		});
	}

	private ApplyOffset() {
		this.x += this._offsetX;
		this.y += this._offsetY;
		this._offsetX = 0;
		this._offsetY = 0;
		this._nodeElement.setAttribute(
			"transform",
			`translate(${this.x},${this.y})`
		);
	}

	public UpdateOutputTargets(targets: number[]) {
		if (this._outputs.length < targets.length) {
			for (let i = this._outputs.length; i < targets.length; ++i) {
				this.AddOutput("");
			}
		} else if (this._outputs.length > targets.length) {
			let overflow = this._outputs.splice(
				targets.length,
				this._outputs.length - targets.length
			);
			overflow.forEach(x => x.Remove());
		}
		this.height = (this._outputs.length + 2) * GBNodeElement.lineheight;
		for (let i = 0; i < this._outputs.length; ++i) {
			this._outputs[i].RemoveAllPaths();
			if (targets[i] >= 0) {
				let targetNode = this.editor.GetNodeElement(targets[i]);
				if (targetNode) {
					this._outputs[i].CreateConnection(targetNode.input);
					targetNode.input.CreateConnection(this._outputs[i]);
				}
			}
		}
		this.UpdateOutputTitles(this.node.GetAttribute("outputTitles"));
	}

	public UpdateOutputTitles(titles: string[]) {
		for (let i = 0; i < titles.length && i < this._outputs.length; ++i) {
			if (titles[i] !== undefined) this._outputs[i].title = titles[i];
		}
	}

	/**
	 * Triggers all visual attributes to update them.
	 */
	public UpdateVisuals() {
		this._node.Trigger("title", undefined, false);
		this._node.Trigger("color", undefined, false);
		this._node.Trigger("textcolor", undefined, false);
		this._node.Trigger("outputTargets", undefined, false);
	}

	public Remove() {
		this.editor.Get(this.id)?.Close();
		this.input.RemoveConnections();
		this._outputs.forEach(x => x.RemoveConnections());
		this.editor.canvas.removeChild(this._nodeElement);
	}

	/* Eventhandlers */

	private DragStart(event: MouseEvent) {
		if (event.button != 0) return;
		event.preventDefault();
		event.stopPropagation();
		this.editor.Blur();
		this._nodeElement.parentNode?.appendChild(this._nodeElement);
		let _movelistener = (e: MouseEvent) =>
			this.Drag(e, event.pageX, event.pageY);
		let _droplistener = (e: MouseEvent) => {
			this.DragEnd(e, event.pageX, event.pageY, _movelistener);
			document.removeEventListener("mouseup", _droplistener);
		};
		document.addEventListener("mousemove", _movelistener);
		document.addEventListener("mouseup", _droplistener);
	}

	private Drag(event: MouseEvent, startX: number, startY: number) {
		if (event.button != 0) return;
		this.offsetX = (event.pageX - startX) / this._editor.zoom;
		this.offsetY = (event.pageY - startY) / this._editor.zoom;
		this.UpdatePaths();
	}

	private DragEnd(
		event: MouseEvent,
		startX: number,
		startY: number,
		draglistener: (e: MouseEvent) => void
	) {
		document.removeEventListener("mousemove", draglistener);
		this.UpdatePaths();
		this.ApplyOffset();
	}

	/* Accessors */

	get id(): number {
		return this.node.GetAttribute("id");
	}

	set title(title: string) {
		this.node.SetAttribute("title", title);
	}

	get title(): string {
		return this.node.GetAttribute("title");
	}

	set color(color: string) {
		this.node.SetAttribute("color", color);
	}

	set x(x: number) {
		this._nodeElement.setAttribute("transform", `translate(${x},${this.y})`);
		this.node.SetAttribute("x", x);
	}

	get x(): number {
		return this.node.GetAttribute("x");
	}

	set y(y: number) {
		this._nodeElement.setAttribute("transform", `translate(${this.x},${y})`);
		this.node.SetAttribute("y", y);
	}

	get y(): number {
		return this.node.GetAttribute("y");
	}

	set width(width: number) {
		this._width = width;
		this._nodeBody.setAttribute("width", width.toString());
		this._nodeDivider.setAttribute("x2", width.toString());
	}

	get width(): number {
		return this._width;
	}

	set height(height: number) {
		this._height = height;
		this._nodeBody.setAttribute("height", height.toString());
	}

	get height(): number {
		return this._height;
	}

	set offsetX(x: number) {
		this._offsetX = x;
		this._nodeElement.setAttribute(
			"transform",
			`translate(${this.x + x},${this.y + this._offsetY})`
		);
	}

	get offsetX(): number {
		return this._offsetX;
	}

	set offsetY(y: number) {
		this._offsetY = y;
		this._nodeElement.setAttribute(
			"transform",
			`translate(${this.x + this._offsetX},${this.y + y})`
		);
	}

	get offsetY(): number {
		return this._offsetY;
	}

	get connectorGElement(): SVGGElement {
		return this._outputContainer;
	}

	get editor(): GBEditor {
		return this._editor;
	}

	get outputCount(): number {
		return this._outputs.length;
	}

	get input(): GBNodeConnector {
		return this._input;
	}

	get node(): GBEditorNode {
		return this._node;
	}
}

