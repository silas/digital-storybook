import {GBNodeElement} from "./GBNodeElement";
import {GBMenu} from "../common/Menu/GBMenu";
import {GBEditorNode} from "../common/Node/GBEditorNode";
import {GBMediaNode} from "../common/Node/GBMediaNode";
import {GBAttributeEditor} from "./GBAttributeEditor";
import {GBSave} from "../common/GBSave";
import {GBLang} from "../language/GBLang";
import {GBMenubar} from "../common/Menu/GBMenubar";
import {GBMenuItem} from "../common/Menu/GBMenuItem";
import {GBNodeConnector} from "./GBNodeConnector";
import {GBNodeContainer} from "../common/GBNodeContainer";
import {GBImageNode} from "../common/Node/MediaNodes/GBImageNode";
import {GBVideoNode} from "../common/Node/MediaNodes/GBVideoNode";
import {GBAudioNode} from "../common/Node/MediaNodes/GBAudioNode";

export class GBEditor {
    public static readonly SUBMENU_NODES = "NODE";
    public static readonly SUBMENU_MEDIAS = "MEDIA";
    public static readonly SUBMENU_VIEW = "VIEW";

    private readonly ns_svg = "http://www.w3.org/2000/svg";
    private root: HTMLDivElement;
    private svg: SVGSVGElement;
    private _canvas: SVGGElement;
    private _sidebarRight: HTMLDivElement;
    private _menubar: GBMenubar;

    private _viewX: number = 0;
    private _viewY: number = 0;
    private _offsetX: number = 0;
    private _offsetY: number = 0;
    private _zoom: number = 1;

    // Node management
    private nodeCounter: number = 1;
    private nodeElements: { [id: number]: GBNodeElement } = {};

    private connectionPreview: SVGPathElement;
    public readonly connectionContainer: SVGGElement;
    private connectionStart: GBNodeConnector | null = null;

    // Image management
    private mediaMenu: GBMenu;
    private media: { [id: number]: { img: GBMediaNode; item: GBMenuItem } } = {};
    private imageCounter: number = 1;

    private openNodes: {
        [type: string]: { [id: number]: GBAttributeEditor };
    } = {};

    constructor(container: HTMLElement, language: string = "en") {
        GBLang.SetLanguage(language);
        this.root = document.createElement("div");
        this.root.setAttribute("class", "GBEditor");
        container.appendChild(this.root);

        // Menu bar
        this._menubar = new GBMenubar(this.root, "GBSidebar GBMenubar left");
        let addNode = this._menubar.menu.AddSubMenu(GBLang.tl("addNode", true) as string, GBEditor.SUBMENU_NODES);
        GBNodeContainer.getTemplateNames().forEach(template => {
            addNode.AddMenuItem(GBLang.tl(template, true) as string, () => {
                this.CreateNode(
                    template,
                    GBLang.tl(template, true) as string,
                    this.ScreenMidX,
                    this.ScreenMidY
                );
            });
        });

        this.mediaMenu = this._menubar.menu.AddSubMenu(GBLang.tl("media", true) as string, GBEditor.SUBMENU_MEDIAS);
        this.mediaMenu.AddMenuItem(GBLang.tl("addImg", true) as string, () => {
            let img = new GBImageNode();
            let id = this.imageCounter++;
            img.SetAttribute("id", id, true);
            img.SetAttribute("tag", GBLang.tl("image", true) as string + id);
            this.CreateMedia(img);
        });
        this.mediaMenu.AddMenuItem(GBLang.tl("addVideo", true) as string, () => {
            let img = new GBVideoNode();
            let id = this.imageCounter++;
            img.SetAttribute("id", id, true);
            img.SetAttribute("tag", GBLang.tl("video", true) as string + id);
            this.CreateMedia(img);
        });
        this.mediaMenu.AddMenuItem(GBLang.tl("addAudio", true) as string, () => {
            let img = new GBAudioNode();
            let id = this.imageCounter++;
            img.SetAttribute("id", id, true);
            img.SetAttribute("tag", GBLang.tl("audio", true) as string + id);
            this.CreateMedia(img);
        });
        let viewMenu = this._menubar.menu.AddSubMenu(GBLang.tl("view", true) as string, GBEditor.SUBMENU_VIEW);
        viewMenu.AddMenuItem(GBLang.tl("toStart", true) as string, () => this.ViewToStart());
        viewMenu.AddMenuItem(GBLang.tl("resetZoom", true) as string, () => (this.zoom = 1));

        // Attribute editor sidebar
        this._sidebarRight = document.createElement("div");
        this._sidebarRight.setAttribute("class", "GBSidebar right");
        this.root.appendChild(this._sidebarRight);

        this.svg = document.createElementNS(this.ns_svg, "svg");
        this.root.appendChild(this.svg);

        this._canvas = document.createElementNS(this.ns_svg, "g");
        this.svg.appendChild(this.canvas);

        this.connectionContainer = document.createElementNS(this.ns_svg, "g");
        this._canvas.appendChild(this.connectionContainer);

        this.connectionPreview = document.createElementNS(this.ns_svg, "path");
        this.connectionPreview.setAttribute("style", "display: none");
        this._canvas.appendChild(this.connectionPreview);

        // Eventlisteners
        this.svg.addEventListener("dragstart", e => {
            e.preventDefault;
        });
        this.svg.addEventListener("mousedown", e => this.DragStart(e));
        this.svg.addEventListener("wheel", e => this.Zoom(e));
        this.svg.addEventListener("contextmenu", e => {
            e.preventDefault();
            e.stopPropagation();
            this.menubar.Toggle();
        });

        // Autosave
        // if (localStorage) {
        //     let save = localStorage.getItem("GB.editor");
        //     if (save) this.Load(save);
        //     else this.CreateStartNode();
        // let _saveListener = () => {
        //     localStorage.setItem("GB.editor", this.Save());
        //     setTimeout(_saveListener, 10000);
        // };
        // setTimeout(_saveListener, 10000);
        // } else {
        // }

        this.CreateStartNode();
        this.ViewToStart();
    }

    /* Methods */

    /**
     * Create a node from a template.
     * @param template Template name
     * @param title Node title. Does not overwrite readonly titles
     * @param x X coordinate
     * @param y Y coordinate
     */
    public CreateNode(
        template: string,
        title: string = "",
        x: number = 0,
        y: number = 0
    ): GBEditorNode {
        let node = GBNodeContainer.constructNode(template);
        node.SetAttribute("title", title);
        node.SetAttribute("x", x);
        node.SetAttribute("y", y);
        node.SetAttribute("id", this.nodeCounter, true);
        this.nodeElements[this.nodeCounter] = new GBNodeElement(this, node);
        return this.nodeElements[this.nodeCounter++].node;
    }

    /**
     * Create a menu item for a media node.
     * @param media Media node
     */
    public CreateMedia(media: GBMediaNode) {
        let tag = media.GetAttribute("tag");
        let id = media.GetAttribute("id");
        let item = this.mediaMenu.AddMenuItem(tag, () => {
            if (!this.IsOpen(id, "image"))
                new GBAttributeEditor(this, media);
            else this.Get(id, "image")?.Highlight();
        });
        media.On("tag", text => (item.text = text));
        this.media[id] = {img: media, item};
    }

    /**
     * Creates/Recreates the start node.
     */
    public CreateStartNode() {
        this.DeleteNode(0);
        let node = GBNodeContainer.constructNode("Relay");
        node.SetAttribute("title", "START");
        node.SetAttribute("id", 0, true);
        node.SetAttributeProperty("title", "readonly", true);
        node.SetAttribute("color", "darkorange");
        node.AddAttribute("prefix", {
            type: "string",
            textarea: true,
            label: GBLang.tl("prefix", true) as string
        });
        node.AddAttribute("init", {
            type: "string",
            textarea: true,
            label: GBLang.tl("init", true) as string
        });
        this.nodeElements[0] = new GBNodeElement(this, node);
    }

    /**
     * Delete a node by ID.
     * @param id ID of node to delete
     */
    public DeleteNode(id: number) {
        let node = this.GetNodeElement(id);
        if (node) {
            node.Remove();
            delete this.nodeElements[id];
        }
    }

    /**
     * Delete a media node by ID.
     * @param id ID of media node to delete
     */
    public DeleteMedia(id: number) {
        if (this.media[id]) {
            if (this.IsOpen(id, "image"))
                this.Get(id, "image")?.Close();
            this.media[id].item.Remove();
            delete this.media[id];
        }
    }

    /**
     * Create a SVG path from one point to another.
     * @param fromX From x coordinate
     * @param fromY From y coordinate
     * @param toX To x coordinate
     * @param toY To y coordinate
     */
    public GetPath(
        fromX: number,
        fromY: number,
        toX: number,
        toY: number
    ): string {
        let midX = 0.5 * (toX + fromX);
        let midY = 0.5 * (toY + fromY);
        return `M${fromX} ${fromY}
				Q${fromX + 0.25 * (toX - fromX)} ${fromY} ${midX} ${midY}
				T${toX} ${toY}`;
    }

    /**
     * Get a graphical node element by ID.
     * @param id ID of node.
     */
    public GetNodeElement(id: number): GBNodeElement | undefined {
        let nodeElement = this.nodeElements[id];
        if (nodeElement) {
            return nodeElement;
        }
        return undefined;
    }

    /**
     * Get Node by ID.
     * @param id ID of node
     */
    public GetNode(id: number): GBEditorNode | undefined {
        if (this.nodeElements[id]) {
            let node = this.nodeElements[id].node;
            if (node) {
                return node;
            }
        }
        return undefined;
    }

    /**
     * Apply the offset to the viewport and reset the offset.
     */
    private ApplyOffset() {
        this._viewX += this._offsetX;
        this._viewY += this._offsetY;
        this._offsetX = 0;
        this._offsetY = 0;
        this.canvas.setAttribute(
            "transform",
            `translate(${this._viewX},${this._viewY}), scale(${this._zoom})`
        );
    }

    /**
     * Center view on start node.
     */
    public ViewToStart() {
        let start = this.GetNode(0);
        this.ScreenMidX = start?.GetAttribute("x");
        this.ScreenMidY = start?.GetAttribute("y");
    }

    /**
     * Clear ALL nodes including start node and reset view.
     */
    public Clear() {
        for (let id in this.nodeElements) {
            this.nodeElements[id].Remove();
            delete this.nodeElements[id];
        }
        for (let i in this.media) {
            this.DeleteMedia(<number>(<unknown>i));
        }
        this.nodeCounter = 1;
        this.imageCounter = 1;
        this._viewX = 0;
        this._viewY = 0;
        this._zoom = 1;
        this._offsetX = 0;
        this._offsetY = 0;
        this.ApplyOffset();
    }

    /**
     * Reset Editor by clearing, recreating start node and centering on said node.
     */
    public Reset() {
        this.Clear();
        this.CreateStartNode();
        this.ViewToStart();
    }

    /**
     * Serialize gamebook to JSON string.
     * @returns JSON serialization of gamebook
     */
    public Save(asJSON?): string | any {

        let save: GBSave = {
            n: [],
            nc: this.nodeCounter,
            i: [],
            ic: this.imageCounter
        };
        for (let id in this.nodeElements) {
            let node = this.nodeElements[id].node;
            save.n.push({t: node.templateName, a: node.Serialize()});
        }
        for (let id in this.media) {
            let img = this.media[id].img;
            save.i.push(img.Serialize());
        }
        if (asJSON) {
            return save;
        }

        return JSON.stringify(save);
    }

    /**
     * Deserialize a gamebook from a JSON string.
     * @param json JSON serialization of a gamebook
     */
    public Load(json: string | GBSave) {
        this.Clear();
        let save: GBSave;
        if (typeof json === "string") {
            save = JSON.parse(json);
        } else {
            save = json;
        }
        this.nodeCounter = save.nc;
        this.imageCounter = save.ic;
        save.n.forEach(n => {
            let node = GBNodeContainer.constructNode(n.t);
            node.Deserialize(n.a);
            this.nodeElements[node.GetAttribute("id")] = new GBNodeElement(
                this,
                node
            );
        });
        save.i.forEach(i => {
            let img = new GBMediaNode();
            img.Deserialize(i);
            this.CreateMedia(img);
        });

        // Trigger connection update
        for (let node in this.nodeElements) {
            this.nodeElements[node].node.Trigger("outputTargets");
        }
    }

    /* Eventhandlers */

    /// Viewport translation
    private DragStart(event: MouseEvent) {
        if (event.button != 0) return;
        event.preventDefault();
        this.Blur();
        let _movelistener = (e: MouseEvent) =>
            this.Drag(e, event.pageX, event.pageY);
        let _droplistener = (e: MouseEvent) => {
            this.DragEnd(e, event.pageX, event.pageY, _movelistener);
            document.removeEventListener("mouseup", _droplistener);
        };

        document.addEventListener("mousemove", _movelistener);
        document.addEventListener("mouseup", _droplistener);
    }

    private Drag(event: MouseEvent, startX: number, startY: number) {
        if (event.button != 0) return;
        this.offsetX = event.pageX - startX;
        this.offsetY = event.pageY - startY;
    }

    private DragEnd(
        event: MouseEvent,
        startX: number,
        startY: number,
        draglistener: (e: MouseEvent) => void
    ) {
        document.removeEventListener("mousemove", draglistener);
        this.ApplyOffset();
    }

    /// Removes focus
    Blur() {
        if (document.activeElement) {
            if (typeof (<HTMLElement>document.activeElement).blur == "function") {
                (<HTMLElement>document.activeElement).blur();
            }
        }
    }

    /// Connection creation
    public ConnectStart(event: MouseEvent, connector: GBNodeConnector) {
        if (event.button != 0) return;
        event.preventDefault();
        event.stopPropagation();
        this.Blur();
        this.connectionStart = connector;
        let _movelistener = (e: MouseEvent) =>
            this.ConnectMove(e, event.pageX, event.pageY, connector.x, connector.y);
        let _droplistener = () => {
            this.connectionStart = null;
            document.removeEventListener("mousemove", _movelistener);
            document.removeEventListener("mouseup", _droplistener);
            this.connectionPreview.setAttribute("style", "display: none");
        };
        this.connectionPreview.setAttribute("d", "");
        this.connectionPreview.setAttribute("style", "");
        document.addEventListener("mousemove", _movelistener);
        document.addEventListener("mouseup", _droplistener);
    }

    private ConnectMove(
        event: MouseEvent,
        startX: number,
        startY: number,
        outX: number,
        outY: number
    ) {
        if (event.button != 0) return;
        let toX =
            (event.pageX - this.svg.getBoundingClientRect().left - this._viewX) /
            this.zoom;
        let toY =
            (event.pageY - this.svg.getBoundingClientRect().top - this._viewY) /
            this.zoom;
        this.connectionPreview.setAttribute(
            "d",
            this.GetPath(outX, outY, toX, toY)
        );
    }

    public ConnectEnd(event: MouseEvent, connector: GBNodeConnector) {
        if (event.button != 0) return;
        event.preventDefault();
        if (this.connectionStart !== null)
            this.Connect(this.connectionStart, connector);
    }

    public Connect(connector1: GBNodeConnector, connector2: GBNodeConnector) {
        let input: GBNodeConnector | null = null;
        let output: GBNodeConnector | null = null;
        if (connector1.input) {
            input = connector1;
            if (!connector2.input) output = connector2;
        } else {
            output = connector1;
            if (connector2.input) input = connector2;
        }

        if (
            input &&
            output &&
            input.node != output.node &&
            !output.IsConnectedTo(input)
        ) {
            output.node.node.ListAttributeSet(
                "outputTargets",
                output.index,
                input.node.id
            );
        }
    }

    /// Zooming
    private Zoom(event: WheelEvent) {
        event.preventDefault();
        event.stopPropagation();
        let nextZoom = Math.max(
            0.1,
            this.zoom * (1 - 0.04 * (event.deltaY / Math.abs(event.deltaY)))
        );
        let deltaZoom = 1 - nextZoom / this.zoom;
        this._viewX -= (this._viewX + this._offsetX - event.pageX) * deltaZoom;
        this._viewY -= (this._viewY + this._offsetY - event.pageY) * deltaZoom;
        this.zoom = nextZoom;
    }


    //NodeEditor
    IsOpen(id: number, type: string = "editor") {
        if (this.openNodes[type] === undefined) return false;
        return this.openNodes[type][id] !== undefined;
    }

    GetOpenEditors(
        type: string = "editor"
    ): { [id: number]: GBAttributeEditor } {
        if (this.openNodes[type] === undefined) this.openNodes[type] = {};
        return this.openNodes[type];
    }

    GetCount(): number {
        let count = 0;
        for (let type in this.openNodes) {
            count += Object.keys(this.openNodes[type]).length;
        }
        return count;
    }

    Get(
        id: number,
        type: string = "editor"
    ): GBAttributeEditor | undefined {
        let editors = this.GetOpenEditors(type);
        return editors[id];
    }

    CloseAll() {
        for (let type in this.openNodes) {
            for (let id in this.openNodes[type])
                this.openNodes[type][id].Close();
        }
    }

    /* Accessors */

    set viewX(x: number) {
        this._viewX = x;
        this.ApplyOffset();
    }

    get viewX(): number {
        return this._viewX;
    }

    set viewY(y: number) {
        this._viewY = y;
        this.ApplyOffset();
    }

    get viewY(): number {
        return this._viewY;
    }

    set offsetX(x: number) {
        this._offsetX = x;
        this.canvas.setAttribute(
            "transform",
            `translate(${this._viewX + x},${this._viewY + this._offsetY}), scale(${
                this._zoom
            })`
        );
    }

    get offsetX(): number {
        return this._offsetX;
    }

    set offsetY(y: number) {
        this._offsetY = y;
        this.canvas.setAttribute(
            "transform",
            `translate(${this._viewX + this._offsetX},${this._viewY + y}), scale(${
                this._zoom
            })`
        );
    }

    get offsetY(): number {
        return this._offsetY;
    }

    set zoom(zoom: number) {
        this._zoom = zoom;
        this.canvas.setAttribute(
            "transform",
            `translate(${this._viewX + this._offsetX},${this._viewY +
            this._offsetY}), scale(${this._zoom})`
        );
    }

    get zoom(): number {
        return this._zoom;
    }

    get canvas(): SVGGElement {
        return this._canvas;
    }

    get sidebarRight(): HTMLElement {
        return this._sidebarRight;
    }

    get menubar(): GBMenubar {
        return this._menubar;
    }

    set ScreenMidX(midX: number) {
        this.viewX = -midX + 0.5 * this.svg.getBoundingClientRect().width;
    }

    get ScreenMidX(): number {
        return (
            -(this._viewX / this.zoom) +
            0.5 * (this.svg.getBoundingClientRect().width / this.zoom)
        );
    }

    set ScreenMidY(midY: number) {
        this.viewY = -midY + 0.5 * this.svg.getBoundingClientRect().height;
    }

    get ScreenMidY(): number {
        return (
            -(this._viewY / this.zoom) +
            0.5 * (this.svg.getBoundingClientRect().height / this.zoom)
        );
    }
}
