const langfile = require("./GBLang.json");

export class GBLang {
    static lang = "en";

    private static translationCallback;

    /**
     * Translate a string with GBLang.json
     * @param text string identifier
     * @param asText
     */
    public static tl(text: string, asText?: boolean): string|Element {
        let result: any = text;
        if (this.translationCallback) {
            return this.translationCallback(text, asText, GBLang.lang);
        } else if (langfile[GBLang.lang][text] !== undefined) {
            result = langfile[GBLang.lang][text];
        }
        if (!asText){
            result = document.createTextNode(text);
        }
        return result;
    }

    public static setTranslationCallback(callback) {
        this.translationCallback = callback;
    }

    /**
     * Set the current language.
     * @param tag localisation tag
     */
    public static SetLanguage(tag: string) {
        if (langfile[tag] !== undefined) GBLang.lang = tag;
    }
}
