import {GBLang} from "../../language/GBLang";
import {GBEditorNode} from "./GBEditorNode";

export class GBEndNode extends GBEditorNode {
    constructor() {
        super();
        this.AddAttribute("rating", {
            type: "string",
            label: GBLang.tl("rating", true) as string,
            textarea: true
        });
        this.SetAttribute("title", GBLang.tl("end", true));
        this.SetAttributeProperty("title", "readonly", true);
        this.SetAttribute("color", "darkgrey");
        this.SetAttribute("textcolor", "black");
    }
}
