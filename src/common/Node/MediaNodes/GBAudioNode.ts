import {GBMediaNode} from "../GBMediaNode";

export class GBAudioNode extends GBMediaNode{

    constructor() {
        super();
        this.SetAttribute("media", "Audio");
        this.AddAttribute("url", {
            type: "file",
            label: "File",
            "audio":true,
            "name":"select Audio",
        });
    }
}