import {GBMediaNode} from "../GBMediaNode";

export class GBImageNode extends GBMediaNode{

    constructor() {
        super();
        this.SetAttribute("media", "Image");
        this.AddAttribute("url", {
            type: "file",
            label: "File",
            "image":true,
            "name":"select Image"
        });
    }
}