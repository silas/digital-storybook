import {GBMediaNode} from "../GBMediaNode";

export class GBVideoNode extends GBMediaNode{

    constructor() {
        super();
        this.SetAttribute("media", "Video");
        this.AddAttribute("url", {
            type: "file",
            label: "File",
            "video":true,
            "name":"select Video"
        });
        this.AddAttribute("subtitles", {
            type: "file",
            label: "File",
            "accept":".vtt",
            "name":"select Subtitles"
        });
    }
}