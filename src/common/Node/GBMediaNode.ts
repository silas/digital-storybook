
// Media nodes
import {GBLang} from "../../language/GBLang";
import {GBNode} from "./GBNode";

export class GBMediaNode extends GBNode {
    constructor() {
        super();
        this.SetAttribute("type", "image", true);
        this.SetAttribute("color", "orange");
        this.SetAttribute("textcolor", "black");
        this.AddAttribute("tag", {
            type: "string",
            label: GBLang.tl("mediaTag", true) as string,
            regExp: "^[a-zA-Z_]\\w*$"
        });
        this.AddAttribute("media", {
            type: "select",
            hidden: true,
            selectChoices: ["Image", "Audio", "Video"]
        });
        this.AddAttribute(
            "alt",
            {
                type: "string",
                label: GBLang.tl("alt", true) as string,
                textarea: true,
            },
            ""
        );
        this.AddAttribute("stretch", {
            type: "boolean",
            label: GBLang.tl("stretch", true) as string,
        });
        this.AddAttribute("center", {
            type: "boolean",
            label: GBLang.tl("center", true) as string,
        });
        this.AddAttribute("inline", {
            type: "boolean",
            label: GBLang.tl("inline", true) as string,
        });
    }
}
