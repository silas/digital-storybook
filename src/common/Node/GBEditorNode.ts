
// Parent class of all editor nodes (visual nodes with input, outputs, title, etc...)
import {GBLang} from "../../language/GBLang";
import {GBNode} from "./GBNode";

export abstract class GBEditorNode extends GBNode {
    constructor() {
        super();
        this.SetAttribute("type", "editor", true);
        this.AddAttribute("title", {
            label: GBLang.tl("title", true) as string,
            type: "string"
        });
        this.AddAttribute("x", {
            type: "number",
            hidden: true
        });
        this.AddAttribute("y", {
            type: "number",
            hidden: true
        });
        this.AddAttribute("outputTitles", {
            type: "list",
            hidden: true,
            listType: "string"
        });
        this.AddAttribute("outputTargets", {
            type: "list",
            hidden: true,
            listType: "number"
        });
    }

    /* Output management */

    /**
     * Adds an output to the node.
     * @param title Title of the added output.
     */
    AddOutput(title: string = "") {
        let i = this.ListAttributePush("outputTargets", -1) - 1;
        this.ListAttributeSet("outputTitles", i, title);
    }

    /**
     * Removes an output from the node.
     * @param index Index of the output.
     */
    RemoveOutput(index: number) {
        this.ListAttributeRemove("outputTargets", index);
        this.ListAttributeRemove("outputTitles", index);
    }

    /**
     * Removes the last output.
     */
    RemoveLastOutput() {
        let i = this.ListAttributeLength("outputTargets") - 1;
        this.ListAttributePop("outputTargets");
        this.ListAttributeRemove("outputTitles", i);
    }

    /**
     * Connect an output with the input of another node.
     * @param index Index of the output.
     * @param targetId ID of the target node. Nodes cannot connect to themselves.
     */
    ConnectOutput(index: number, targetId: number) {
        if (this.ListAttributeLength("outputTargets") > index) {
            if (this.GetAttribute("id") != targetId)
                this.ListAttributeSet("outputTargets", index, targetId);
        }
    }

    /**
     * Disconnect an output. Does nothing if output was not connected.
     * @param index Index of output to disconnect.
     */
    DisconnectOutput(index: number) {
        if (this.ListAttributeLength("outputTargets") > index) {
            this.ListAttributeSet("outputTargets", index, -1);
        }
    }
}