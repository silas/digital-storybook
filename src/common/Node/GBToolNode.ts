
// Parent class of tool nodes
import {GBEditorNode} from "./GBEditorNode";

export abstract class GBToolNode extends GBEditorNode {
    constructor() {
        super();
        this.SetAttributeProperty("title", "readonly", true);
        this.SetAttribute("color", "darkgreen");
    }
    /**
     * Executes toolnode. select function must be called with a valid output id.
     * @param scope Variable register.
     * @returns Selected output.
     */
    abstract Execute(scope: { [key: string]: any }): number;
}
