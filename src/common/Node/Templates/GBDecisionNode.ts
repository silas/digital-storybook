import {GBRoomNode} from "../GBRoomNode";
import {GBLang} from "../../../language/GBLang";
import {GBRender} from "../../GBRender";
import {GBNode} from "../GBNode";

export class GBDecisionNode extends GBRoomNode {
    constructor() {
        super();
        this.AddAttribute("decisions", {
            label: GBLang.tl("decisions", true) as string,
            type: "list",
            listType: "string",
            textarea: true
        });
        this.AddAttribute("shuffle", {
            type: "boolean",
            label: GBLang.tl("shuffle", true) as string
        });
        this.On("decisions", decisions => this.Update(decisions));
    }

    Update(decisions: string[]) {
        let diff = decisions.length - this.ListAttributeLength("outputTargets");
        if (diff > 0) {
            while (diff-- > 0) {
                this.AddOutput();
            }
        } else {
            while (diff++ < 0) {
                this.RemoveLastOutput();
            }
        }
        this.SetAttribute("outputTitles", this.GetAttribute("decisions"));
        this.Trigger("outputTitles", decisions, false);
    }

    Render(
        select: (output: number) => void,
        scope: { [key: string]: any },
        renderTarget: HTMLElement
    ): void {
        renderTarget.innerText = "";
        renderTarget.appendChild(GBRender.GetRenderer().RenderRoom(
            this.GetAttribute("room"),
            scope
        ));
        let decisions = <string[]>this.GetAttribute("decisions");
        let _createButton = (decision: string, index: number) => {
            let button = document.createElement("input");
            button.setAttribute("type", "button");
            button.setAttribute(
                "value",
                GBRender.GetRenderer().SubstituteMath(decision, scope)
            );
            button.addEventListener("click", () => {
                button.setAttribute("style", "background-color: lightblue");
                button.value = GBLang.tl("pressed", true) + ": " + button.value;
                select(index);
            });
            renderTarget.appendChild(button);
        };
        if (this.GetAttribute("shuffle")) {
            GBNode.ShuffleForEach(decisions, _createButton);
        } else {
            decisions.forEach(_createButton);
        }
    }
}