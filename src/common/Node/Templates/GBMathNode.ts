import {GBToolNode} from "../GBToolNode";
import {GBLang} from "../../../language/GBLang";
import {SecureEvaluate} from "../../GBMath";

export class GBMathNode extends GBToolNode {
    constructor() {
        super();
        this.AddAttribute("expression", {
            type: "string",
            label: GBLang.tl("expression", true) as string,
            textarea: true
        });
        this.AddOutput("Output");
        this.SetAttribute("title", GBLang.tl("math", true), true);
    }
    Execute(scope: { [key: string]: any }): number {
        scope["ans"] = SecureEvaluate(this.GetAttribute("expression"), scope);
        return 0;
    }
}