import {GBRoomNode} from "../GBRoomNode";
import {GBLang} from "../../../language/GBLang";
import {GBRender} from "../../GBRender";

export class GBCustomNode extends GBRoomNode {
    constructor() {
        super();
        this.AddAttribute("inputs", {
            label: GBLang.tl("inputTypes", true) as string,
            type: "list",
            listType: "select",
            selectChoices: ["String", "Number", "Boolean"]
        });
        this.AddAttribute("labels", {
            label: GBLang.tl("inputLabels", true) as string,
            type: "list",
            listType: "string"
        });
        this.AddAttribute("submit", {
            label: GBLang.tl("submitText", true) as string,
            type: "string"
        });
        this.AddOutput("Output");
    }
    Render(
        select: (output: number) => void,
        scope: { [key: string]: any },
        renderTarget: HTMLElement
    ): void {
        renderTarget.innerText = "";
        renderTarget.appendChild(GBRender.GetRenderer().RenderRoom(
            this.GetAttribute("room"),
            scope
        ));
        let inputs = this.GetAttribute("inputs") as string[];
        let labels = this.GetAttribute("labels") as string[];
        let ans: { [key: number]: any } = {};
        inputs.forEach((type, index) => {
            let input = document.createElement("input");
            if (type == "Number") {
                input.type = "number";
                input.value = "0";
                ans[index] = 0;
            } else if (type == "Boolean") {
                input.type = "checkbox";
                ans[index] = false;
            } else {
                input.type = "text";
                ans[index] = "";
            }
            let label = document.createElement("label");
            if (labels[index] !== undefined) {
                if (type == "Boolean") {
                    label.appendChild(input);
                    label.appendChild(document.createTextNode(" " + labels[index]));
                } else {
                    label.appendChild(document.createTextNode(labels[index] + ": "));
                    label.appendChild(input);
                }
            } else {
                label.appendChild(input);
            }
            input.addEventListener("change", () => {
                if (type == "Boolean") {
                    ans[index] = input.checked;
                } else {
                    ans[index] = input.value;
                }
            });
            renderTarget.appendChild(label);
        });
        let submit = document.createElement("input");
        submit.setAttribute("type", "button");
        submit.setAttribute("value", this.GetAttribute("submit"));
        submit.addEventListener("click", () => {
            scope.ans = ans;
            select(0);
            console.log(scope);
        });
        renderTarget.appendChild(submit);
    }
}