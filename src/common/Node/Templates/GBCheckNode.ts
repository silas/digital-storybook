import {GBToolNode} from "../GBToolNode";
import {GBLang} from "../../../language/GBLang";
import {SecureEvaluate} from "../../GBMath";

export class GBCheckNode extends GBToolNode {
    constructor() {
        super();
        this.AddAttribute("expression", {
            type: "string",
            label: GBLang.tl("expression", true) as string
        });
        this.AddOutput(GBLang.tl("true", true) as string);
        this.AddOutput(GBLang.tl("false", true) as string);
        this.SetAttribute("title", GBLang.tl("Check", true), true);
    }
    Execute(scope: { [key: string]: any }): number {
        if (SecureEvaluate(this.GetAttribute("expression"), scope)) {
            return 0;
        } else return 1;
    }
}