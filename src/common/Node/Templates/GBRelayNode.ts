import {GBToolNode} from "../GBToolNode";
import {GBLang} from "../../../language/GBLang";

export class GBRelayNode extends GBToolNode {
    constructor() {
        super();
        this.AddOutput("Output");
        this.SetAttributeProperty("title", "readonly", false);
        this.SetAttribute("title", GBLang.tl("relay", true));
    }
    Execute(): number {
        return 0;
    }
}