import {GBRoomNode} from "../GBRoomNode";
import {GBLang} from "../../../language/GBLang";
import {GBRender} from "../../GBRender";
import {GBNode} from "../GBNode";

export class GBMultipleChoiceNode extends GBRoomNode {
    constructor() {
        super();
        this.AddAttribute("true", {
            label: GBLang.tl("right", true) as string,
            type: "list",
            listType: "string",
            textarea: true
        });
        this.AddAttribute("false", {
            label: GBLang.tl("wrong", true) as string,
            type: "list",
            listType: "string",
            textarea: true
        });
        this.AddAttribute("single", {
            label: GBLang.tl("single", true) as string,
            type: "boolean"
        });
        this.AddAttribute(
            "submit",
            {
                label: GBLang.tl("submitText", true) as string,
                type: "string"
            },
            GBLang.tl("submit", true)
        );
        this.AddOutput(GBLang.tl("correct", true) as string);
        this.AddOutput(GBLang.tl("incorrect", true) as string);
    }

    Render(
        select: (output: number) => void,
        scope: { [key: string]: any },
        renderTarget: HTMLElement
    ): void {
        renderTarget.innerText = "";
        renderTarget.appendChild(GBRender.GetRenderer().RenderRoom(
            this.GetAttribute("room"),
            scope
        ));
        let correct = this.GetAttribute("true") as string[];
        let incorrect = this.GetAttribute("false") as string[];
        let choices = correct.concat(incorrect);
        let result: boolean[] = new Array(choices.length);
        let single: boolean = this.GetAttribute("single");
        GBNode.ShuffleForEach(choices, (choice, index) => {
            let input = document.createElement("input");
            input.id = "mc" + index;
            if (single) {
                input.setAttribute("type", "radio");
                input.setAttribute("name", "mc");
            } else {
                input.setAttribute("type", "checkbox");
            }
            let label = document.createElement("label");
            label.setAttribute("for", input.id);
            input.addEventListener("change", () => {
                result[index] = input.checked;
            });
            label.appendChild(input);
            label.appendChild(
                document.createTextNode(
                    GBRender.GetRenderer().SubstituteMath(choice, scope)
                )
            );
            renderTarget.appendChild(label);
        });
        let submit = document.createElement("input");
        submit.setAttribute("type", "button");
        submit.setAttribute("value", this.GetAttribute("submit"));
        submit.addEventListener("click", () => {
            if (single) {
                let ans: { [key: number]: boolean; points: number } = {points: 0};
                for (let i = 0; i < result.length; ++i) {
                    ans[i] = result[i] ? true : false;
                    if (i < correct.length && result[i]) {
                        ans.points = 1;
                    }
                    if (i >= correct.length && result[i]) {
                        ans.points = -1;
                    }
                    scope.ans = ans;
                }
                if (ans.points == 1) select(0);
                else select(1);
            } else {
                let ans: { [key: number]: boolean; points: number } = {points: 0};
                for (let i = 0; i < result.length; ++i) {
                    ans[i] = result[i] ? true : false;
                    if (i < correct.length) {
                        if (result[i]) ans.points++;
                        else ans.points--;
                    } else {
                        if (result[i]) ans.points--;
                        else ans.points++;
                    }
                }
                scope.ans = ans;
                if (ans.points == choices.length) select(0);
                else select(1);
            }
        });
        renderTarget.appendChild(submit);
    }
}