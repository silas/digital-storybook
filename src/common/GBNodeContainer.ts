import {GBEditorNode} from "./Node/GBEditorNode";
import {GBEndNode} from "./Node/GBEndNode";
import {GBRelayNode} from "./Node/Templates/GBRelayNode";
import {GBCheckNode} from "./Node/Templates/GBCheckNode";
import {GBMathNode} from "./Node/Templates/GBMathNode";
import {GBCustomNode} from "./Node/Templates/GBCustomNode";
import {GBMultipleChoiceNode} from "./Node/Templates/GBMultipleChoiceNode";
import {GBDecisionNode} from "./Node/Templates/GBDecisionNode";

/**
 * Node templates are defined here. To add a new template, just add a new node instance to the object,
 * define its attributes in its constructor after calling the super constructor and then implement
 * all inherited abstract methods.
 *
 * GBRoomNode needs a Render method which gets a select callback, a scope and a renderTarget.
 * Render everything you want the player to see to the rendertarget and bind the select callback with
 * the wanted output index to an input event. It is advised to first render the room description.
 *
 * GBToolNode needs an Execute method which gets a scope and returns a number. The scope contains all
 * variables in the current gamebook. Calculate what you want and return the output index that should be
 * chosen.
 *
 * Look at existing templates as examples.
 */
export class GBNodeContainer {

    private static TEMPLATES: { [key: string]: any } = {
        Decision: GBDecisionNode,
        "Multiple choice": GBMultipleChoiceNode,
        Custom: GBCustomNode,
        Math: GBMathNode,
        Check: GBCheckNode,
        Relay: GBRelayNode,
        end: GBEndNode
    };

    static constructNode(template: string): GBEditorNode {
        const node = new GBNodeContainer.TEMPLATES[template]();
        node.templateName = template;
        return node;
    }

    static setTemplate(name, classObject){
        GBNodeContainer.TEMPLATES[name] = classObject;
    }

    static getTemplateNames() {
        return Object.keys(GBNodeContainer.TEMPLATES);
    }
}