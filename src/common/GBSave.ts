/**
 * Structure to serialize gamebooks
 */
export interface GBSave {
	// Nodes, contains template and attributes
	n: { t: string; a: string }[];
	// Nodecount
	nc: number;
	// Images
	i: string[];
	// Imagecount
	ic: number;
}
