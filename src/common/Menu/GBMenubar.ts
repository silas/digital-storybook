import {GBMenu} from "./GBMenu";

export class GBMenubar {
    private style: string;

    private menubar: HTMLDivElement;
    private _menu: GBMenu;

    private _open = false;

    constructor(container: HTMLElement, style: string = "") {
        this.style = style;
        this.menubar = document.createElement("div");
        this.menubar.setAttribute("class", this.style);

        this._menu = new GBMenu(this.menubar);

        container.appendChild(this.menubar);
    }

    Open() {
        this.menubar.setAttribute("class", this.style + " open");
        this._open = true;
    }

    Close() {
        this.menubar.setAttribute("class", this.style);
        this._open = false;
    }

    Toggle() {
        if (this.open) this.Close();
        else this.Open();
    }

    get open(): boolean {
        return this._open;
    }

    get menu(): GBMenu {
        return this._menu;
    }
}