export class GBMenuItem {
    private container: HTMLElement;
    private callback: { (e: MouseEvent): void } = function() {};

    private item: HTMLLIElement;
    private anchor: HTMLAnchorElement;

    constructor(
        container: HTMLElement,
        text: string = "",
        callback?: { (event: MouseEvent): void }
    ) {
        this.container = container;
        this.item = document.createElement("li");
        this.anchor = document.createElement("a");
        this.anchor.setAttribute("tabindex", "0");
        this.text = text;

        if (callback) this.SetCallback(callback);

        this.item.appendChild(this.anchor);
        this.container.appendChild(this.item);
    }

    /**
     * Set the callback method of the menu item
     * @param callback Callback method
     */
    SetCallback(callback: { (e: MouseEvent): void }) {
        this.anchor.removeEventListener("click", this.callback);
        this.callback = (e: MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            callback(e);
        };
        this.anchor.addEventListener("click", this.callback);
    }

    /**
     * Remove menu item from menu
     */
    Remove() {
        this.container.removeChild(this.item);
    }

    /// Accessors

    set text(text: string) {
        this.anchor.textContent = text;
    }

    get text(): string {
        return this.anchor.textContent ? this.anchor.textContent : "";
    }
}