import { GBNodeElement } from "./GBNodeElement";
export declare class GBNodeConnector {
    private readonly _node;
    readonly input: boolean;
    private _connectorElement;
    private _titleElement;
    private _connectedTo;
    private _connectionPath;
    private _title;
    private _index;
    constructor(node: GBNodeElement, input?: boolean, title?: string);
    CreateConnection(to: GBNodeConnector): void;
    RemovePath(to: GBNodeConnector, propagate?: boolean): void;
    RemoveAllPaths(): void;
    Remove(): void;
    IsConnectedTo(to: GBNodeConnector): boolean;
    UpdatePaths(): void;
    RemoveConnections(): void;
    set path(to: GBNodeConnector | null);
    set index(i: number);
    get index(): number;
    set title(title: string);
    get title(): string;
    get x(): number;
    get y(): number;
    get node(): GBNodeElement;
    set color(color: string);
}
