import { GBNode } from "../common/Node/GBNode";
import { GBEditor } from "./GBEditor";
export declare class GBAttributeEditor {
    private sidebar;
    private node;
    private editor;
    private nodeEditor;
    constructor(nodeEditor: GBEditor, node: GBNode);
    Close(): void;
    Highlight(): void;
    AddControl(id: string): void;
    AddNumberInput(row: HTMLDivElement, id: string): void;
    AddFileInput(row: HTMLDivElement, id: string): void;
    AddStringInput(row: HTMLDivElement, id: string): void;
    AddBooleanInput(row: HTMLDivElement, id: string): void;
    AddSelectInput(row: HTMLDivElement, id: string): void;
    AddListControl(row: HTMLDivElement, id: string): void;
    AddListEntry(id: string): HTMLDivElement;
    AddListInput(id: string, index: number, value: any): HTMLDivElement;
    private UpdateIndices;
}
