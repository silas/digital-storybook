import { GBNodeElement } from "./GBNodeElement";
import { GBEditorNode } from "../common/Node/GBEditorNode";
import { GBMediaNode } from "../common/Node/GBMediaNode";
import { GBAttributeEditor } from "./GBAttributeEditor";
import { GBSave } from "../common/GBSave";
import { GBMenubar } from "../common/Menu/GBMenubar";
import { GBNodeConnector } from "./GBNodeConnector";
export declare class GBEditor {
    static readonly SUBMENU_NODES = "NODE";
    static readonly SUBMENU_MEDIAS = "MEDIA";
    static readonly SUBMENU_VIEW = "VIEW";
    private readonly ns_svg;
    private root;
    private svg;
    private _canvas;
    private _sidebarRight;
    private _menubar;
    private _viewX;
    private _viewY;
    private _offsetX;
    private _offsetY;
    private _zoom;
    private nodeCounter;
    private nodeElements;
    private connectionPreview;
    readonly connectionContainer: SVGGElement;
    private connectionStart;
    private mediaMenu;
    private media;
    private imageCounter;
    private openNodes;
    constructor(container: HTMLElement, language?: string);
    /**
     * Create a node from a template.
     * @param template Template name
     * @param title Node title. Does not overwrite readonly titles
     * @param x X coordinate
     * @param y Y coordinate
     */
    CreateNode(template: string, title?: string, x?: number, y?: number): GBEditorNode;
    /**
     * Create a menu item for a media node.
     * @param media Media node
     */
    CreateMedia(media: GBMediaNode): void;
    /**
     * Creates/Recreates the start node.
     */
    CreateStartNode(): void;
    /**
     * Delete a node by ID.
     * @param id ID of node to delete
     */
    DeleteNode(id: number): void;
    /**
     * Delete a media node by ID.
     * @param id ID of media node to delete
     */
    DeleteMedia(id: number): void;
    /**
     * Create a SVG path from one point to another.
     * @param fromX From x coordinate
     * @param fromY From y coordinate
     * @param toX To x coordinate
     * @param toY To y coordinate
     */
    GetPath(fromX: number, fromY: number, toX: number, toY: number): string;
    /**
     * Get a graphical node element by ID.
     * @param id ID of node.
     */
    GetNodeElement(id: number): GBNodeElement | undefined;
    /**
     * Get Node by ID.
     * @param id ID of node
     */
    GetNode(id: number): GBEditorNode | undefined;
    /**
     * Apply the offset to the viewport and reset the offset.
     */
    private ApplyOffset;
    /**
     * Center view on start node.
     */
    ViewToStart(): void;
    /**
     * Clear ALL nodes including start node and reset view.
     */
    Clear(): void;
    /**
     * Reset Editor by clearing, recreating start node and centering on said node.
     */
    Reset(): void;
    /**
     * Serialize gamebook to JSON string.
     * @returns JSON serialization of gamebook
     */
    Save(asJSON?: any): string | any;
    /**
     * Deserialize a gamebook from a JSON string.
     * @param json JSON serialization of a gamebook
     */
    Load(json: string | GBSave): void;
    private DragStart;
    private Drag;
    private DragEnd;
    Blur(): void;
    ConnectStart(event: MouseEvent, connector: GBNodeConnector): void;
    private ConnectMove;
    ConnectEnd(event: MouseEvent, connector: GBNodeConnector): void;
    Connect(connector1: GBNodeConnector, connector2: GBNodeConnector): void;
    private Zoom;
    IsOpen(id: number, type?: string): boolean;
    GetOpenEditors(type?: string): {
        [id: number]: GBAttributeEditor;
    };
    GetCount(): number;
    Get(id: number, type?: string): GBAttributeEditor | undefined;
    CloseAll(): void;
    set viewX(x: number);
    get viewX(): number;
    set viewY(y: number);
    get viewY(): number;
    set offsetX(x: number);
    get offsetX(): number;
    set offsetY(y: number);
    get offsetY(): number;
    set zoom(zoom: number);
    get zoom(): number;
    get canvas(): SVGGElement;
    get sidebarRight(): HTMLElement;
    get menubar(): GBMenubar;
    set ScreenMidX(midX: number);
    get ScreenMidX(): number;
    set ScreenMidY(midY: number);
    get ScreenMidY(): number;
}
