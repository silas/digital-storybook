import { GBEditor } from "./GBEditor";
import { GBEditorNode } from "../common/Node/GBEditorNode";
import { GBNodeConnector } from "./GBNodeConnector";
export declare class GBNodeElement {
    static readonly ns_svg = "http://www.w3.org/2000/svg";
    static readonly nodewidth: number;
    static readonly lineheight: number;
    private _editor;
    private _nodeElement;
    private _nodeBody;
    private _nodeDivider;
    private _nodeTitle;
    private _outputContainer;
    private _node;
    private _offsetX;
    private _offsetY;
    private _width;
    private _height;
    private _input;
    private _outputs;
    constructor(editor: GBEditor, node: GBEditorNode);
    AddOutput(title: string): GBNodeConnector;
    UpdatePaths(): void;
    private ApplyOffset;
    UpdateOutputTargets(targets: number[]): void;
    UpdateOutputTitles(titles: string[]): void;
    /**
     * Triggers all visual attributes to update them.
     */
    UpdateVisuals(): void;
    Remove(): void;
    private DragStart;
    private Drag;
    private DragEnd;
    get id(): number;
    set title(title: string);
    get title(): string;
    set color(color: string);
    set x(x: number);
    get x(): number;
    set y(y: number);
    get y(): number;
    set width(width: number);
    get width(): number;
    set height(height: number);
    get height(): number;
    set offsetX(x: number);
    get offsetX(): number;
    set offsetY(y: number);
    get offsetY(): number;
    get connectorGElement(): SVGGElement;
    get editor(): GBEditor;
    get outputCount(): number;
    get input(): GBNodeConnector;
    get node(): GBEditorNode;
}
