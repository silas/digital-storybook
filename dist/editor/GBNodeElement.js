"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBNodeElement = void 0;
const GBAttributeEditor_1 = require("./GBAttributeEditor");
const GBNodeConnector_1 = require("./GBNodeConnector");
class GBNodeElement {
    constructor(editor, node) {
        this._offsetX = 0;
        this._offsetY = 0;
        this._width = 0;
        this._height = 0;
        this._outputs = [];
        this._node = node;
        // Create elements and set constant attributes
        this._nodeElement = document.createElementNS(GBNodeElement.ns_svg, "g");
        this._nodeElement.setAttribute("class", "GBNode");
        this._nodeDivider = document.createElementNS(GBNodeElement.ns_svg, "line");
        this._nodeDivider.setAttribute("x1", "0");
        this._nodeDivider.setAttribute("y1", GBNodeElement.lineheight.toString());
        this._nodeDivider.setAttribute("y2", GBNodeElement.lineheight.toString());
        this._nodeBody = document.createElementNS(GBNodeElement.ns_svg, "rect");
        this._nodeBody.setAttribute("rx", "10px");
        this._nodeBody.setAttribute("ry", "10px");
        this._nodeTitle = document.createElementNS(GBNodeElement.ns_svg, "text");
        this._nodeTitle.setAttribute("transform", "translate(10,15)");
        this._outputContainer = document.createElementNS(GBNodeElement.ns_svg, "g");
        this._outputContainer.setAttribute("transform", `translate(0,${GBNodeElement.lineheight})`);
        this._input = new GBNodeConnector_1.GBNodeConnector(this, true, "Input");
        this._input.index = -1;
        // Set properties
        this._editor = editor;
        this.x = node.GetAttribute("x");
        this.y = node.GetAttribute("y");
        this.width = GBNodeElement.nodewidth;
        this.height = GBNodeElement.lineheight * 2;
        // Add to DOM
        this._nodeElement.appendChild(this._nodeBody);
        this._nodeElement.appendChild(this._nodeDivider);
        this._nodeElement.appendChild(this._nodeTitle);
        this._nodeElement.appendChild(this._outputContainer);
        editor.canvas.appendChild(this._nodeElement);
        // Add events
        this._nodeElement.addEventListener("mousedown", e => this.DragStart(e));
        this._nodeElement.addEventListener("contextmenu", e => {
            var _a;
            e.preventDefault();
            e.stopPropagation();
            if (!editor.IsOpen(this.id)) {
                new GBAttributeEditor_1.GBAttributeEditor(this.editor, this._node);
            }
            else {
                (_a = editor.Get(this.id)) === null || _a === void 0 ? void 0 : _a.Highlight();
            }
        });
        this._node.On("title", (title) => {
            if (title.length > 16) {
                title = title.substr(0, 16);
                title += "\u{2026}";
            }
            this._nodeTitle.textContent = title;
        });
        this._node.On("color", c => (this._nodeBody.style["fill"] = c));
        this._node.On("textcolor", c => {
            this._nodeTitle.style["fill"] = c;
            this._input.color = c;
            this._outputs.forEach(x => (x.color = c));
        });
        this._node.On("outputTargets", x => this.UpdateOutputTargets(x), false);
        this._node.On("outputTitles", x => this.UpdateOutputTitles(x), false);
        this.UpdateVisuals();
    }
    /* Methods */
    AddOutput(title) {
        // Prepare DOM elements
        let connector = new GBNodeConnector_1.GBNodeConnector(this, false, title);
        // Push to array
        this._outputs.push(connector);
        this.height += GBNodeElement.lineheight;
        return connector;
    }
    UpdatePaths() {
        this._input.UpdatePaths();
        this._outputs.forEach(connector => {
            connector.UpdatePaths();
        });
    }
    ApplyOffset() {
        this.x += this._offsetX;
        this.y += this._offsetY;
        this._offsetX = 0;
        this._offsetY = 0;
        this._nodeElement.setAttribute("transform", `translate(${this.x},${this.y})`);
    }
    UpdateOutputTargets(targets) {
        if (this._outputs.length < targets.length) {
            for (let i = this._outputs.length; i < targets.length; ++i) {
                this.AddOutput("");
            }
        }
        else if (this._outputs.length > targets.length) {
            let overflow = this._outputs.splice(targets.length, this._outputs.length - targets.length);
            overflow.forEach(x => x.Remove());
        }
        this.height = (this._outputs.length + 2) * GBNodeElement.lineheight;
        for (let i = 0; i < this._outputs.length; ++i) {
            this._outputs[i].RemoveAllPaths();
            if (targets[i] >= 0) {
                let targetNode = this.editor.GetNodeElement(targets[i]);
                if (targetNode) {
                    this._outputs[i].CreateConnection(targetNode.input);
                    targetNode.input.CreateConnection(this._outputs[i]);
                }
            }
        }
        this.UpdateOutputTitles(this.node.GetAttribute("outputTitles"));
    }
    UpdateOutputTitles(titles) {
        for (let i = 0; i < titles.length && i < this._outputs.length; ++i) {
            if (titles[i] !== undefined)
                this._outputs[i].title = titles[i];
        }
    }
    /**
     * Triggers all visual attributes to update them.
     */
    UpdateVisuals() {
        this._node.Trigger("title", undefined, false);
        this._node.Trigger("color", undefined, false);
        this._node.Trigger("textcolor", undefined, false);
        this._node.Trigger("outputTargets", undefined, false);
    }
    Remove() {
        var _a;
        (_a = this.editor.Get(this.id)) === null || _a === void 0 ? void 0 : _a.Close();
        this.input.RemoveConnections();
        this._outputs.forEach(x => x.RemoveConnections());
        this.editor.canvas.removeChild(this._nodeElement);
    }
    /* Eventhandlers */
    DragStart(event) {
        var _a;
        if (event.button != 0)
            return;
        event.preventDefault();
        event.stopPropagation();
        this.editor.Blur();
        (_a = this._nodeElement.parentNode) === null || _a === void 0 ? void 0 : _a.appendChild(this._nodeElement);
        let _movelistener = (e) => this.Drag(e, event.pageX, event.pageY);
        let _droplistener = (e) => {
            this.DragEnd(e, event.pageX, event.pageY, _movelistener);
            document.removeEventListener("mouseup", _droplistener);
        };
        document.addEventListener("mousemove", _movelistener);
        document.addEventListener("mouseup", _droplistener);
    }
    Drag(event, startX, startY) {
        if (event.button != 0)
            return;
        this.offsetX = (event.pageX - startX) / this._editor.zoom;
        this.offsetY = (event.pageY - startY) / this._editor.zoom;
        this.UpdatePaths();
    }
    DragEnd(event, startX, startY, draglistener) {
        document.removeEventListener("mousemove", draglistener);
        this.UpdatePaths();
        this.ApplyOffset();
    }
    /* Accessors */
    get id() {
        return this.node.GetAttribute("id");
    }
    set title(title) {
        this.node.SetAttribute("title", title);
    }
    get title() {
        return this.node.GetAttribute("title");
    }
    set color(color) {
        this.node.SetAttribute("color", color);
    }
    set x(x) {
        this._nodeElement.setAttribute("transform", `translate(${x},${this.y})`);
        this.node.SetAttribute("x", x);
    }
    get x() {
        return this.node.GetAttribute("x");
    }
    set y(y) {
        this._nodeElement.setAttribute("transform", `translate(${this.x},${y})`);
        this.node.SetAttribute("y", y);
    }
    get y() {
        return this.node.GetAttribute("y");
    }
    set width(width) {
        this._width = width;
        this._nodeBody.setAttribute("width", width.toString());
        this._nodeDivider.setAttribute("x2", width.toString());
    }
    get width() {
        return this._width;
    }
    set height(height) {
        this._height = height;
        this._nodeBody.setAttribute("height", height.toString());
    }
    get height() {
        return this._height;
    }
    set offsetX(x) {
        this._offsetX = x;
        this._nodeElement.setAttribute("transform", `translate(${this.x + x},${this.y + this._offsetY})`);
    }
    get offsetX() {
        return this._offsetX;
    }
    set offsetY(y) {
        this._offsetY = y;
        this._nodeElement.setAttribute("transform", `translate(${this.x + this._offsetX},${this.y + y})`);
    }
    get offsetY() {
        return this._offsetY;
    }
    get connectorGElement() {
        return this._outputContainer;
    }
    get editor() {
        return this._editor;
    }
    get outputCount() {
        return this._outputs.length;
    }
    get input() {
        return this._input;
    }
    get node() {
        return this._node;
    }
}
exports.GBNodeElement = GBNodeElement;
GBNodeElement.ns_svg = "http://www.w3.org/2000/svg";
GBNodeElement.nodewidth = 180;
GBNodeElement.lineheight = 22;
//# sourceMappingURL=GBNodeElement.js.map