"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBEditor = void 0;
const GBNodeElement_1 = require("./GBNodeElement");
const GBMediaNode_1 = require("../common/Node/GBMediaNode");
const GBAttributeEditor_1 = require("./GBAttributeEditor");
const GBLang_1 = require("../language/GBLang");
const GBMenubar_1 = require("../common/Menu/GBMenubar");
const GBNodeContainer_1 = require("../common/GBNodeContainer");
const GBImageNode_1 = require("../common/Node/MediaNodes/GBImageNode");
const GBVideoNode_1 = require("../common/Node/MediaNodes/GBVideoNode");
const GBAudioNode_1 = require("../common/Node/MediaNodes/GBAudioNode");
class GBEditor {
    constructor(container, language = "en") {
        this.ns_svg = "http://www.w3.org/2000/svg";
        this._viewX = 0;
        this._viewY = 0;
        this._offsetX = 0;
        this._offsetY = 0;
        this._zoom = 1;
        // Node management
        this.nodeCounter = 1;
        this.nodeElements = {};
        this.connectionStart = null;
        this.media = {};
        this.imageCounter = 1;
        this.openNodes = {};
        GBLang_1.GBLang.SetLanguage(language);
        this.root = document.createElement("div");
        this.root.setAttribute("class", "GBEditor");
        container.appendChild(this.root);
        // Menu bar
        this._menubar = new GBMenubar_1.GBMenubar(this.root, "GBSidebar GBMenubar left");
        let addNode = this._menubar.menu.AddSubMenu(GBLang_1.GBLang.tl("addNode", true), GBEditor.SUBMENU_NODES);
        GBNodeContainer_1.GBNodeContainer.getTemplateNames().forEach(template => {
            addNode.AddMenuItem(GBLang_1.GBLang.tl(template, true), () => {
                this.CreateNode(template, GBLang_1.GBLang.tl(template, true), this.ScreenMidX, this.ScreenMidY);
            });
        });
        this.mediaMenu = this._menubar.menu.AddSubMenu(GBLang_1.GBLang.tl("media", true), GBEditor.SUBMENU_MEDIAS);
        this.mediaMenu.AddMenuItem(GBLang_1.GBLang.tl("addImg", true), () => {
            let img = new GBImageNode_1.GBImageNode();
            let id = this.imageCounter++;
            img.SetAttribute("id", id, true);
            img.SetAttribute("tag", GBLang_1.GBLang.tl("image", true) + id);
            this.CreateMedia(img);
        });
        this.mediaMenu.AddMenuItem(GBLang_1.GBLang.tl("addVideo", true), () => {
            let img = new GBVideoNode_1.GBVideoNode();
            let id = this.imageCounter++;
            img.SetAttribute("id", id, true);
            img.SetAttribute("tag", GBLang_1.GBLang.tl("video", true) + id);
            this.CreateMedia(img);
        });
        this.mediaMenu.AddMenuItem(GBLang_1.GBLang.tl("addAudio", true), () => {
            let img = new GBAudioNode_1.GBAudioNode();
            let id = this.imageCounter++;
            img.SetAttribute("id", id, true);
            img.SetAttribute("tag", GBLang_1.GBLang.tl("audio", true) + id);
            this.CreateMedia(img);
        });
        let viewMenu = this._menubar.menu.AddSubMenu(GBLang_1.GBLang.tl("view", true), GBEditor.SUBMENU_VIEW);
        viewMenu.AddMenuItem(GBLang_1.GBLang.tl("toStart", true), () => this.ViewToStart());
        viewMenu.AddMenuItem(GBLang_1.GBLang.tl("resetZoom", true), () => (this.zoom = 1));
        // Attribute editor sidebar
        this._sidebarRight = document.createElement("div");
        this._sidebarRight.setAttribute("class", "GBSidebar right");
        this.root.appendChild(this._sidebarRight);
        this.svg = document.createElementNS(this.ns_svg, "svg");
        this.root.appendChild(this.svg);
        this._canvas = document.createElementNS(this.ns_svg, "g");
        this.svg.appendChild(this.canvas);
        this.connectionContainer = document.createElementNS(this.ns_svg, "g");
        this._canvas.appendChild(this.connectionContainer);
        this.connectionPreview = document.createElementNS(this.ns_svg, "path");
        this.connectionPreview.setAttribute("style", "display: none");
        this._canvas.appendChild(this.connectionPreview);
        // Eventlisteners
        this.svg.addEventListener("dragstart", e => {
            e.preventDefault;
        });
        this.svg.addEventListener("mousedown", e => this.DragStart(e));
        this.svg.addEventListener("wheel", e => this.Zoom(e));
        this.svg.addEventListener("contextmenu", e => {
            e.preventDefault();
            e.stopPropagation();
            this.menubar.Toggle();
        });
        // Autosave
        // if (localStorage) {
        //     let save = localStorage.getItem("GB.editor");
        //     if (save) this.Load(save);
        //     else this.CreateStartNode();
        // let _saveListener = () => {
        //     localStorage.setItem("GB.editor", this.Save());
        //     setTimeout(_saveListener, 10000);
        // };
        // setTimeout(_saveListener, 10000);
        // } else {
        // }
        this.CreateStartNode();
        this.ViewToStart();
    }
    /* Methods */
    /**
     * Create a node from a template.
     * @param template Template name
     * @param title Node title. Does not overwrite readonly titles
     * @param x X coordinate
     * @param y Y coordinate
     */
    CreateNode(template, title = "", x = 0, y = 0) {
        let node = GBNodeContainer_1.GBNodeContainer.constructNode(template);
        node.SetAttribute("title", title);
        node.SetAttribute("x", x);
        node.SetAttribute("y", y);
        node.SetAttribute("id", this.nodeCounter, true);
        this.nodeElements[this.nodeCounter] = new GBNodeElement_1.GBNodeElement(this, node);
        return this.nodeElements[this.nodeCounter++].node;
    }
    /**
     * Create a menu item for a media node.
     * @param media Media node
     */
    CreateMedia(media) {
        let tag = media.GetAttribute("tag");
        let id = media.GetAttribute("id");
        let item = this.mediaMenu.AddMenuItem(tag, () => {
            var _a;
            if (!this.IsOpen(id, "image"))
                new GBAttributeEditor_1.GBAttributeEditor(this, media);
            else
                (_a = this.Get(id, "image")) === null || _a === void 0 ? void 0 : _a.Highlight();
        });
        media.On("tag", text => (item.text = text));
        this.media[id] = { img: media, item };
    }
    /**
     * Creates/Recreates the start node.
     */
    CreateStartNode() {
        this.DeleteNode(0);
        let node = GBNodeContainer_1.GBNodeContainer.constructNode("Relay");
        node.SetAttribute("title", "START");
        node.SetAttribute("id", 0, true);
        node.SetAttributeProperty("title", "readonly", true);
        node.SetAttribute("color", "darkorange");
        node.AddAttribute("prefix", {
            type: "string",
            textarea: true,
            label: GBLang_1.GBLang.tl("prefix", true)
        });
        node.AddAttribute("init", {
            type: "string",
            textarea: true,
            label: GBLang_1.GBLang.tl("init", true)
        });
        this.nodeElements[0] = new GBNodeElement_1.GBNodeElement(this, node);
    }
    /**
     * Delete a node by ID.
     * @param id ID of node to delete
     */
    DeleteNode(id) {
        let node = this.GetNodeElement(id);
        if (node) {
            node.Remove();
            delete this.nodeElements[id];
        }
    }
    /**
     * Delete a media node by ID.
     * @param id ID of media node to delete
     */
    DeleteMedia(id) {
        var _a;
        if (this.media[id]) {
            if (this.IsOpen(id, "image"))
                (_a = this.Get(id, "image")) === null || _a === void 0 ? void 0 : _a.Close();
            this.media[id].item.Remove();
            delete this.media[id];
        }
    }
    /**
     * Create a SVG path from one point to another.
     * @param fromX From x coordinate
     * @param fromY From y coordinate
     * @param toX To x coordinate
     * @param toY To y coordinate
     */
    GetPath(fromX, fromY, toX, toY) {
        let midX = 0.5 * (toX + fromX);
        let midY = 0.5 * (toY + fromY);
        return `M${fromX} ${fromY}
				Q${fromX + 0.25 * (toX - fromX)} ${fromY} ${midX} ${midY}
				T${toX} ${toY}`;
    }
    /**
     * Get a graphical node element by ID.
     * @param id ID of node.
     */
    GetNodeElement(id) {
        let nodeElement = this.nodeElements[id];
        if (nodeElement) {
            return nodeElement;
        }
        return undefined;
    }
    /**
     * Get Node by ID.
     * @param id ID of node
     */
    GetNode(id) {
        if (this.nodeElements[id]) {
            let node = this.nodeElements[id].node;
            if (node) {
                return node;
            }
        }
        return undefined;
    }
    /**
     * Apply the offset to the viewport and reset the offset.
     */
    ApplyOffset() {
        this._viewX += this._offsetX;
        this._viewY += this._offsetY;
        this._offsetX = 0;
        this._offsetY = 0;
        this.canvas.setAttribute("transform", `translate(${this._viewX},${this._viewY}), scale(${this._zoom})`);
    }
    /**
     * Center view on start node.
     */
    ViewToStart() {
        let start = this.GetNode(0);
        this.ScreenMidX = start === null || start === void 0 ? void 0 : start.GetAttribute("x");
        this.ScreenMidY = start === null || start === void 0 ? void 0 : start.GetAttribute("y");
    }
    /**
     * Clear ALL nodes including start node and reset view.
     */
    Clear() {
        for (let id in this.nodeElements) {
            this.nodeElements[id].Remove();
            delete this.nodeElements[id];
        }
        for (let i in this.media) {
            this.DeleteMedia(i);
        }
        this.nodeCounter = 1;
        this.imageCounter = 1;
        this._viewX = 0;
        this._viewY = 0;
        this._zoom = 1;
        this._offsetX = 0;
        this._offsetY = 0;
        this.ApplyOffset();
    }
    /**
     * Reset Editor by clearing, recreating start node and centering on said node.
     */
    Reset() {
        this.Clear();
        this.CreateStartNode();
        this.ViewToStart();
    }
    /**
     * Serialize gamebook to JSON string.
     * @returns JSON serialization of gamebook
     */
    Save(asJSON) {
        let save = {
            n: [],
            nc: this.nodeCounter,
            i: [],
            ic: this.imageCounter
        };
        for (let id in this.nodeElements) {
            let node = this.nodeElements[id].node;
            save.n.push({ t: node.templateName, a: node.Serialize() });
        }
        for (let id in this.media) {
            let img = this.media[id].img;
            save.i.push(img.Serialize());
        }
        if (asJSON) {
            return save;
        }
        return JSON.stringify(save);
    }
    /**
     * Deserialize a gamebook from a JSON string.
     * @param json JSON serialization of a gamebook
     */
    Load(json) {
        this.Clear();
        let save;
        if (typeof json === "string") {
            save = JSON.parse(json);
        }
        else {
            save = json;
        }
        this.nodeCounter = save.nc;
        this.imageCounter = save.ic;
        save.n.forEach(n => {
            let node = GBNodeContainer_1.GBNodeContainer.constructNode(n.t);
            node.Deserialize(n.a);
            this.nodeElements[node.GetAttribute("id")] = new GBNodeElement_1.GBNodeElement(this, node);
        });
        save.i.forEach(i => {
            let img = new GBMediaNode_1.GBMediaNode();
            img.Deserialize(i);
            this.CreateMedia(img);
        });
        // Trigger connection update
        for (let node in this.nodeElements) {
            this.nodeElements[node].node.Trigger("outputTargets");
        }
    }
    /* Eventhandlers */
    /// Viewport translation
    DragStart(event) {
        if (event.button != 0)
            return;
        event.preventDefault();
        this.Blur();
        let _movelistener = (e) => this.Drag(e, event.pageX, event.pageY);
        let _droplistener = (e) => {
            this.DragEnd(e, event.pageX, event.pageY, _movelistener);
            document.removeEventListener("mouseup", _droplistener);
        };
        document.addEventListener("mousemove", _movelistener);
        document.addEventListener("mouseup", _droplistener);
    }
    Drag(event, startX, startY) {
        if (event.button != 0)
            return;
        this.offsetX = event.pageX - startX;
        this.offsetY = event.pageY - startY;
    }
    DragEnd(event, startX, startY, draglistener) {
        document.removeEventListener("mousemove", draglistener);
        this.ApplyOffset();
    }
    /// Removes focus
    Blur() {
        if (document.activeElement) {
            if (typeof document.activeElement.blur == "function") {
                document.activeElement.blur();
            }
        }
    }
    /// Connection creation
    ConnectStart(event, connector) {
        if (event.button != 0)
            return;
        event.preventDefault();
        event.stopPropagation();
        this.Blur();
        this.connectionStart = connector;
        let _movelistener = (e) => this.ConnectMove(e, event.pageX, event.pageY, connector.x, connector.y);
        let _droplistener = () => {
            this.connectionStart = null;
            document.removeEventListener("mousemove", _movelistener);
            document.removeEventListener("mouseup", _droplistener);
            this.connectionPreview.setAttribute("style", "display: none");
        };
        this.connectionPreview.setAttribute("d", "");
        this.connectionPreview.setAttribute("style", "");
        document.addEventListener("mousemove", _movelistener);
        document.addEventListener("mouseup", _droplistener);
    }
    ConnectMove(event, startX, startY, outX, outY) {
        if (event.button != 0)
            return;
        let toX = (event.pageX - this.svg.getBoundingClientRect().left - this._viewX) /
            this.zoom;
        let toY = (event.pageY - this.svg.getBoundingClientRect().top - this._viewY) /
            this.zoom;
        this.connectionPreview.setAttribute("d", this.GetPath(outX, outY, toX, toY));
    }
    ConnectEnd(event, connector) {
        if (event.button != 0)
            return;
        event.preventDefault();
        if (this.connectionStart !== null)
            this.Connect(this.connectionStart, connector);
    }
    Connect(connector1, connector2) {
        let input = null;
        let output = null;
        if (connector1.input) {
            input = connector1;
            if (!connector2.input)
                output = connector2;
        }
        else {
            output = connector1;
            if (connector2.input)
                input = connector2;
        }
        if (input &&
            output &&
            input.node != output.node &&
            !output.IsConnectedTo(input)) {
            output.node.node.ListAttributeSet("outputTargets", output.index, input.node.id);
        }
    }
    /// Zooming
    Zoom(event) {
        event.preventDefault();
        event.stopPropagation();
        let nextZoom = Math.max(0.1, this.zoom * (1 - 0.04 * (event.deltaY / Math.abs(event.deltaY))));
        let deltaZoom = 1 - nextZoom / this.zoom;
        this._viewX -= (this._viewX + this._offsetX - event.pageX) * deltaZoom;
        this._viewY -= (this._viewY + this._offsetY - event.pageY) * deltaZoom;
        this.zoom = nextZoom;
    }
    //NodeEditor
    IsOpen(id, type = "editor") {
        if (this.openNodes[type] === undefined)
            return false;
        return this.openNodes[type][id] !== undefined;
    }
    GetOpenEditors(type = "editor") {
        if (this.openNodes[type] === undefined)
            this.openNodes[type] = {};
        return this.openNodes[type];
    }
    GetCount() {
        let count = 0;
        for (let type in this.openNodes) {
            count += Object.keys(this.openNodes[type]).length;
        }
        return count;
    }
    Get(id, type = "editor") {
        let editors = this.GetOpenEditors(type);
        return editors[id];
    }
    CloseAll() {
        for (let type in this.openNodes) {
            for (let id in this.openNodes[type])
                this.openNodes[type][id].Close();
        }
    }
    /* Accessors */
    set viewX(x) {
        this._viewX = x;
        this.ApplyOffset();
    }
    get viewX() {
        return this._viewX;
    }
    set viewY(y) {
        this._viewY = y;
        this.ApplyOffset();
    }
    get viewY() {
        return this._viewY;
    }
    set offsetX(x) {
        this._offsetX = x;
        this.canvas.setAttribute("transform", `translate(${this._viewX + x},${this._viewY + this._offsetY}), scale(${this._zoom})`);
    }
    get offsetX() {
        return this._offsetX;
    }
    set offsetY(y) {
        this._offsetY = y;
        this.canvas.setAttribute("transform", `translate(${this._viewX + this._offsetX},${this._viewY + y}), scale(${this._zoom})`);
    }
    get offsetY() {
        return this._offsetY;
    }
    set zoom(zoom) {
        this._zoom = zoom;
        this.canvas.setAttribute("transform", `translate(${this._viewX + this._offsetX},${this._viewY +
            this._offsetY}), scale(${this._zoom})`);
    }
    get zoom() {
        return this._zoom;
    }
    get canvas() {
        return this._canvas;
    }
    get sidebarRight() {
        return this._sidebarRight;
    }
    get menubar() {
        return this._menubar;
    }
    set ScreenMidX(midX) {
        this.viewX = -midX + 0.5 * this.svg.getBoundingClientRect().width;
    }
    get ScreenMidX() {
        return (-(this._viewX / this.zoom) +
            0.5 * (this.svg.getBoundingClientRect().width / this.zoom));
    }
    set ScreenMidY(midY) {
        this.viewY = -midY + 0.5 * this.svg.getBoundingClientRect().height;
    }
    get ScreenMidY() {
        return (-(this._viewY / this.zoom) +
            0.5 * (this.svg.getBoundingClientRect().height / this.zoom));
    }
}
exports.GBEditor = GBEditor;
GBEditor.SUBMENU_NODES = "NODE";
GBEditor.SUBMENU_MEDIAS = "MEDIA";
GBEditor.SUBMENU_VIEW = "VIEW";
//# sourceMappingURL=GBEditor.js.map