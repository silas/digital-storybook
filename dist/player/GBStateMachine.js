"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBStateMachine = void 0;
const GBLang_1 = require("../language/GBLang");
const GBNodeContainer_1 = require("../common/GBNodeContainer");
class GBStateMachine {
    constructor(save = undefined) {
        this.states = {};
        if (save !== undefined) {
            this.Load(save);
            this.currentState = this.states[0];
        }
        else {
            this.currentState = GBNodeContainer_1.GBNodeContainer.constructNode("Decision");
            this.currentState.SetAttribute("room", GBLang_1.GBLang.tl("unloaded", true));
        }
    }
    /**
     * Loads a gamebook
     * @param save Save to load
     */
    Load(save) {
        save.n.forEach(node => {
            let n = GBNodeContainer_1.GBNodeContainer.constructNode(node.t);
            n.Deserialize(node.a);
            let id = n.GetAttribute("id");
            this.states[id] = n;
        });
    }
    /**
     * Gets the current state
     */
    get state() {
        return this.currentState;
    }
    /**
     * Sets the current state by id
     * @param id ID of state
     */
    SetState(id) {
        if (this.states[id] !== undefined) {
            this.currentState = this.states[id];
        }
    }
    /**
     * Transition through a given output by id
     * @param output Output ID
     */
    Transition(output) {
        let id = this.state.ListAttributeGet("outputTargets", output);
        this.currentState = this.states[id];
    }
    /**
     * Fetch a state by ID
     * @param id State ID
     * @returns Requested node
     */
    FetchState(id) {
        return this.states[id];
    }
}
exports.GBStateMachine = GBStateMachine;
//# sourceMappingURL=GBStateMachine.js.map