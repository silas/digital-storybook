import { GBNode } from "../common/Node/GBNode";
import { GBSave } from "../common/GBSave";
export declare class GBStateMachine {
    states: {
        [id: number]: GBNode;
    };
    private currentState;
    constructor(save?: GBSave | undefined);
    /**
     * Loads a gamebook
     * @param save Save to load
     */
    Load(save: GBSave): void;
    /**
     * Gets the current state
     */
    get state(): GBNode;
    /**
     * Sets the current state by id
     * @param id ID of state
     */
    SetState(id: number): void;
    /**
     * Transition through a given output by id
     * @param output Output ID
     */
    Transition(output: number): void;
    /**
     * Fetch a state by ID
     * @param id State ID
     * @returns Requested node
     */
    FetchState(id: number): GBNode;
}
