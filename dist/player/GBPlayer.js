"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBPlayer = void 0;
const GBStateMachine_1 = require("./GBStateMachine");
const GBRoomNode_1 = require("../common/Node/GBRoomNode");
const GBToolNode_1 = require("../common/Node/GBToolNode");
const GBMediaNode_1 = require("../common/Node/GBMediaNode");
const GBEndNode_1 = require("../common/Node/GBEndNode");
const GBRender_1 = require("../common/GBRender");
const GBMath_1 = require("../common/GBMath");
const GBLang_1 = require("../language/GBLang");
const GBMenubar_1 = require("../common/Menu/GBMenubar");
class GBPlayer {
    constructor(container, callback, language = "en") {
        this.pages = [];
        this.currentPage = null;
        this.pageNumber = 0;
        this._fontSize = 24;
        this.nodeRenderedCallback = null;
        this.variableRegister = {};
        GBLang_1.GBLang.SetLanguage(language);
        this.root = document.createElement("div");
        this.root.setAttribute("class", "GBPlayer");
        // Menu
        this.menuContainer = document.createElement("div");
        this.menuContainer.setAttribute("class", "GBMenuContainer");
        this._menu = new GBMenubar_1.GBMenubar(this.menuContainer, "GBMenu");
        this.menu.AddMenuItem(GBLang_1.GBLang.tl("back", true), () => {
            this._menu.Close();
            this.root.removeChild(this.menuContainer);
            this.root.appendChild(this.playerContainer);
        });
        let fontMenu = this.menu.AddSubMenu(GBLang_1.GBLang.tl("font", true));
        fontMenu.AddMenuItem(GBLang_1.GBLang.tl("incSize", true), () => {
            this.fontSize += 2;
        });
        fontMenu.AddMenuItem(GBLang_1.GBLang.tl("decSize", true), () => {
            this.fontSize -= 2;
        });
        // Controlbar
        this.controlBar = document.createElement("div");
        this.controlBar.setAttribute("class", "GBControlBar");
        // Jump to page button
        let button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "^");
        button.setAttribute("aria-label", GBLang_1.GBLang.tl("jumpPage", true));
        button.addEventListener("click", () => {
            let page = Number(prompt(GBLang_1.GBLang.tl("pagePrompt", true) + (this.pages.length + 1)));
            if (page != NaN && page != 0) {
                this.GoToPage(page);
            }
        });
        this.controlBar.appendChild(button);
        // Previous page button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "<");
        button.setAttribute("aria-label", GBLang_1.GBLang.tl("previousPage", true));
        button.addEventListener("click", () => {
            if (this.pageNumber > 1) {
                this.GoToPage(this.pageNumber - 1);
            }
        });
        this.controlBar.appendChild(button);
        // Menu button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "...");
        button.setAttribute("aria-label", GBLang_1.GBLang.tl("menu", true));
        button.addEventListener("click", () => {
            this._menu.Open();
            this.root.removeChild(this.playerContainer);
            this.root.appendChild(this.menuContainer);
        });
        this.controlBar.appendChild(button);
        // Next page button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", ">");
        button.setAttribute("aria-label", GBLang_1.GBLang.tl("nextPage", true));
        button.addEventListener("click", () => {
            if (this.pageNumber <= this.pages.length) {
                this.GoToPage(this.pageNumber + 1);
            }
        });
        this.controlBar.appendChild(button);
        // Current page button
        button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", ">>");
        button.setAttribute("aria-label", GBLang_1.GBLang.tl("currentPage", true));
        button.addEventListener("click", () => {
            this.GoToCurrentPage();
        });
        this.controlBar.appendChild(button);
        // PageContainer and PageContent
        this.pageContainer = document.createElement("div");
        this.pageContainer.setAttribute("class", "GBPageContainer");
        this.pageContent = document.createElement("div");
        this.pageContainer.appendChild(this.pageContent);
        this.playerContainer = document.createElement("div");
        this.playerContainer.appendChild(this.pageContainer);
        this.playerContainer.appendChild(this.controlBar);
        this.root.appendChild(this.playerContainer);
        container.appendChild(this.root);
        this.callback = callback;
        this.stateMachine = new GBStateMachine_1.GBStateMachine();
        this.DoState();
    }
    /**
     * Disable all input elements in a given element.
     * @param element Element in which all inputs should be disabled
     */
    DisableInputs(element) {
        element.querySelectorAll("input").forEach(input => {
            input.disabled = true;
            if (input.type == "checkbox") {
                input.setAttribute("checked", input.checked.toString());
            }
            else {
                input.setAttribute("value", input.value);
            }
        });
    }
    /**
     * Execute the internal logic of the current state.
     */
    DoState() {
        let state = this.stateMachine.state;
        if (state instanceof GBRoomNode_1.GBRoomNode) {
            let container = document.createElement("div");
            state.Render(output => {
                this.Select(output);
            }, this.variableRegister, container);
            this.currentPage = container;
            this.pageContent.appendChild(this.currentPage);
            this.pageContainer.scrollTop = 0;
            this.pageNumber++;
            if (typeof this.nodeRenderedCallback === "function") {
                this.nodeRenderedCallback(container, state);
            }
        }
        else if (state instanceof GBToolNode_1.GBToolNode) {
            this.Select(state.Execute(this.variableRegister));
        }
        else if (state instanceof GBEndNode_1.GBEndNode) {
            this.callback(GBMath_1.SecureEvaluate(state.GetAttribute("rating"), this.variableRegister));
        }
    }
    setNodeRenderedCallback(callback) {
        this.nodeRenderedCallback = callback;
    }
    /**
     * Select an output of the current state.
     * @param output ID of selected output
     */
    Select(output) {
        if (this.currentPage !== null) {
            this.DisableInputs(this.pageContent);
            this.pages.push(this.pageContent.innerHTML);
        }
        this.pageContent.innerHTML = "";
        this.currentPage = null;
        this.stateMachine.Transition(output);
        this.DoState();
    }
    /**
     * Load a gamebook from a GBSave structure.
     * @param save GBSave structure to load
     */
    LoadGamebook(save) {
        this.pageContent.textContent = "";
        this.pages = [];
        this.variableRegister = {};
        this.currentPage = null;
        this.pageNumber = 0;
        this.stateMachine = new GBStateMachine_1.GBStateMachine(save);
        let media = {};
        save.i.forEach(imageString => {
            let image = new GBMediaNode_1.GBMediaNode();
            image.Deserialize(imageString);
            media[image.GetAttribute("tag")] = image;
        });
        let startNode = this.stateMachine.FetchState(0);
        GBMath_1.SecureEvaluate(startNode.GetAttribute("init"), this.variableRegister);
        GBRender_1.GBRender.GetRenderer().SetPrefix(startNode.GetAttribute("prefix"));
        GBRender_1.GBRender.GetRenderer().SetMedia(media);
        this.DoState();
    }
    /**
     * Deserialize JSON and then load it.
     * @param gamebookJson JSON string
     */
    LoadGamebookFromString(gamebookJson) {
        let save = JSON.parse(gamebookJson);
        this.LoadGamebook(save);
    }
    /**
     * Jump to the last page.
     */
    GoToCurrentPage() {
        this.pageNumber = this.pages.length + 1;
        if (this.currentPage !== null) {
            this.pageContent.innerHTML = "";
            this.pageContent.appendChild(this.currentPage);
        }
    }
    /**
     * Jump to a given page.
     * @param page Page to jump to
     */
    GoToPage(page) {
        if (page <= this.pages.length && page > 0) {
            this.pageNumber = page;
            this.pageContent.innerHTML = this.pages[page - 1];
        }
        else {
            this.GoToCurrentPage();
        }
    }
    /**
     * Serialize the current savestate to a JSON string.
     * @returns Savestate JSON
     */
    SaveState() {
        let save = {
            hash: this.HashString(JSON.stringify(this.stateMachine.states) +
                JSON.stringify(this.variableRegister) +
                this.stateMachine.state.GetAttribute("id")),
            vars: this.variableRegister,
            state: this.stateMachine.state.GetAttribute("id"),
            pages: this.pages
        };
        return JSON.stringify(save);
    }
    /**
     * Deserializes a JSON string and loads the savestate.
     * @param state Savestate JSON
     */
    LoadState(state) {
        let save = JSON.parse(state);
        if (this.CheckCompatibility(save)) {
            this.pageContent.innerHTML = "";
            this.stateMachine.SetState(save.state);
            this.pages = save.pages;
            this.variableRegister = save.vars;
            this.pageNumber = this.pages.length;
            this.DoState();
        }
        else {
            this.pageContent.appendChild(GBLang_1.GBLang.tl("compatible"));
        }
    }
    /**
     * A simple hash function.
     * It is used to make it impossible to cross-load savestates.
     * @param text String to hash
     */
    HashString(text) {
        let hash = 0, i, chr;
        for (i = 0; i < text.length; i++) {
            chr = text.charCodeAt(i);
            hash = (hash << 5) - hash + chr;
            hash |= 0;
        }
        return hash;
    }
    /**
     * Check the compatibility of a savestate with the current gamebook.
     * @param save SaveState structure to check compatibility with
     * @returns true if the savestate is compatible, false if not.
     */
    CheckCompatibility(save) {
        return (this.HashString(JSON.stringify(this.stateMachine.states) +
            JSON.stringify(save.vars) +
            save.state) === save.hash);
    }
    getCurrentPageNumber() {
        return this.pageNumber;
    }
    /// Accessors
    get menu() {
        return this._menu.menu;
    }
    get fontSize() {
        return this._fontSize;
    }
    set fontSize(size) {
        if (size > 0) {
            this.root.style["fontSize"] = size + "px";
            this._fontSize = size;
        }
    }
}
exports.GBPlayer = GBPlayer;
//# sourceMappingURL=GBPlayer.js.map