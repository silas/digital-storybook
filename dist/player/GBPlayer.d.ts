import { GBStateMachine } from "./GBStateMachine";
import { GBSave } from "../common/GBSave";
import { GBMenu } from "../common/Menu/GBMenu";
export declare class GBPlayer {
    private root;
    private playerContainer;
    private pageContainer;
    private pageContent;
    private controlBar;
    private pages;
    private currentPage;
    private pageNumber;
    private callback;
    private _menu;
    private menuContainer;
    private _fontSize;
    private nodeRenderedCallback;
    stateMachine: GBStateMachine;
    variableRegister: {
        [key: string]: any;
    };
    constructor(container: HTMLElement, callback: (rating: any) => void, language?: string);
    /**
     * Disable all input elements in a given element.
     * @param element Element in which all inputs should be disabled
     */
    private DisableInputs;
    /**
     * Execute the internal logic of the current state.
     */
    DoState(): void;
    setNodeRenderedCallback(callback: any): void;
    /**
     * Select an output of the current state.
     * @param output ID of selected output
     */
    private Select;
    /**
     * Load a gamebook from a GBSave structure.
     * @param save GBSave structure to load
     */
    LoadGamebook(save: GBSave): void;
    /**
     * Deserialize JSON and then load it.
     * @param gamebookJson JSON string
     */
    LoadGamebookFromString(gamebookJson: string): void;
    /**
     * Jump to the last page.
     */
    GoToCurrentPage(): void;
    /**
     * Jump to a given page.
     * @param page Page to jump to
     */
    GoToPage(page: number): void;
    /**
     * Serialize the current savestate to a JSON string.
     * @returns Savestate JSON
     */
    SaveState(): string;
    /**
     * Deserializes a JSON string and loads the savestate.
     * @param state Savestate JSON
     */
    LoadState(state: string): void;
    /**
     * A simple hash function.
     * It is used to make it impossible to cross-load savestates.
     * @param text String to hash
     */
    private HashString;
    /**
     * Check the compatibility of a savestate with the current gamebook.
     * @param save SaveState structure to check compatibility with
     * @returns true if the savestate is compatible, false if not.
     */
    private CheckCompatibility;
    getCurrentPageNumber(): number;
    get menu(): GBMenu;
    get fontSize(): number;
    set fontSize(size: number);
}
