"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBPlayer = exports.GBEditor = void 0;
// If you do not want to distribute both player and editor, you can comment out or remove one of the lines below.
var GBEditor_1 = require("./editor/GBEditor");
Object.defineProperty(exports, "GBEditor", { enumerable: true, get: function () { return GBEditor_1.GBEditor; } });
var GBPlayer_1 = require("./player/GBPlayer");
Object.defineProperty(exports, "GBPlayer", { enumerable: true, get: function () { return GBPlayer_1.GBPlayer; } });
//# sourceMappingURL=index.js.map