export declare class GBLang {
    static lang: string;
    private static translationCallback;
    /**
     * Translate a string with GBLang.json
     * @param text string identifier
     * @param asText
     */
    static tl(text: string, asText?: boolean): string | Element;
    static setTranslationCallback(callback: any): void;
    /**
     * Set the current language.
     * @param tag localisation tag
     */
    static SetLanguage(tag: string): void;
}
