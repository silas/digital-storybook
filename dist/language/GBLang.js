"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBLang = void 0;
const langfile = require("./GBLang.json");
class GBLang {
    /**
     * Translate a string with GBLang.json
     * @param text string identifier
     * @param asText
     */
    static tl(text, asText) {
        let result = text;
        if (this.translationCallback) {
            return this.translationCallback(text, asText, GBLang.lang);
        }
        else if (langfile[GBLang.lang][text] !== undefined) {
            result = langfile[GBLang.lang][text];
        }
        if (!asText) {
            result = document.createTextNode(text);
        }
        return result;
    }
    static setTranslationCallback(callback) {
        this.translationCallback = callback;
    }
    /**
     * Set the current language.
     * @param tag localisation tag
     */
    static SetLanguage(tag) {
        if (langfile[tag] !== undefined)
            GBLang.lang = tag;
    }
}
exports.GBLang = GBLang;
GBLang.lang = "en";
//# sourceMappingURL=GBLang.js.map