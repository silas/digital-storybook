import { GBEditorNode } from "./GBNode";
export declare function ConstructNode(template: string): GBEditorNode;
export declare function GetTemplateNames(): string[];
