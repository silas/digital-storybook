"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetTemplateNames = exports.ConstructNode = void 0;
const GBLang_1 = require("../language/GBLang");
const GBNode_1 = require("./GBNode");
const GBRender_1 = require("./GBRender");
const GBMath_1 = require("./GBMath");
/**
 * Node templates are defined here. To add a new template, just add a new node instance to the object,
 * define its attributes in its constructor after calling the super constructor and then implement
 * all inherited abstract methods.
 *
 * GBRoomNode needs a Render method which gets a select callback, a scope and a renderTarget.
 * Render everything you want the player to see to the rendertarget and bind the select callback with
 * the wanted output index to an input event. It is advised to first render the room description.
 *
 * GBToolNode needs an Execute method which gets a scope and returns a number. The scope contains all
 * variables in the current gamebook. Calculate what you want and return the output index that should be
 * chosen.
 *
 * Look at existing templates as examples.
 */
let GBNodeTemplates = {
    Decision: class extends GBNode_1.GBRoomNode {
        constructor() {
            super();
            this.AddAttribute("decisions", {
                label: GBLang_1.GBLang.tl("decisions"),
                type: "list",
                listType: "string",
                textarea: true
            });
            this.AddAttribute("shuffle", {
                type: "boolean",
                label: GBLang_1.GBLang.tl("shuffle")
            });
            this.On("decisions", decisions => this.Update(decisions));
        }
        Update(decisions) {
            let diff = decisions.length - this.ListAttributeLength("outputTargets");
            if (diff > 0) {
                while (diff-- > 0) {
                    this.AddOutput();
                }
            }
            else {
                while (diff++ < 0) {
                    this.RemoveLastOutput();
                }
            }
            this.SetAttribute("outputTitles", this.GetAttribute("decisions"));
            this.Trigger("outputTitles", decisions, false);
        }
        Render(select, scope, renderTarget) {
            renderTarget.innerHTML = GBRender_1.GBRender.GetRenderer().RenderRoom(this.GetAttribute("room"), scope);
            let decisions = this.GetAttribute("decisions");
            let _createButton = (decision, index) => {
                let button = document.createElement("input");
                button.setAttribute("type", "button");
                button.setAttribute("value", GBRender_1.GBRender.GetRenderer().SubstituteMath(decision, scope));
                button.addEventListener("click", () => {
                    button.setAttribute("style", "background-color: lightblue");
                    button.value = GBLang_1.GBLang.tl("pressed") + ": " + button.value;
                    select(index);
                });
                renderTarget.appendChild(button);
            };
            if (this.GetAttribute("shuffle"))
                ShuffleForEach(decisions, _createButton);
            else
                decisions.forEach(_createButton);
        }
    },
    "Multiple choice": class extends GBNode_1.GBRoomNode {
        constructor() {
            super();
            this.AddAttribute("true", {
                label: GBLang_1.GBLang.tl("right"),
                type: "list",
                listType: "string",
                textarea: true
            });
            this.AddAttribute("false", {
                label: GBLang_1.GBLang.tl("wrong"),
                type: "list",
                listType: "string",
                textarea: true
            });
            this.AddAttribute("single", {
                label: GBLang_1.GBLang.tl("single"),
                type: "boolean"
            });
            this.AddAttribute("submit", {
                label: GBLang_1.GBLang.tl("submitText"),
                type: "string"
            }, GBLang_1.GBLang.tl("submit"));
            this.AddOutput(GBLang_1.GBLang.tl("correct"));
            this.AddOutput(GBLang_1.GBLang.tl("incorrect"));
        }
        Render(select, scope, renderTarget) {
            renderTarget.innerHTML = GBRender_1.GBRender.GetRenderer().RenderRoom(this.GetAttribute("room"), scope);
            let correct = this.GetAttribute("true");
            let incorrect = this.GetAttribute("false");
            let choices = correct.concat(incorrect);
            let result = new Array(choices.length);
            let single = this.GetAttribute("single");
            ShuffleForEach(choices, (choice, index) => {
                let input = document.createElement("input");
                input.id = "mc" + index;
                if (single) {
                    input.setAttribute("type", "radio");
                    input.setAttribute("name", "mc");
                }
                else {
                    input.setAttribute("type", "checkbox");
                }
                let label = document.createElement("label");
                label.setAttribute("for", input.id);
                input.addEventListener("change", () => {
                    result[index] = input.checked;
                });
                label.appendChild(input);
                label.appendChild(document.createTextNode(GBRender_1.GBRender.GetRenderer().SubstituteMath(choice, scope)));
                renderTarget.appendChild(label);
            });
            let submit = document.createElement("input");
            submit.setAttribute("type", "button");
            submit.setAttribute("value", this.GetAttribute("submit"));
            submit.addEventListener("click", () => {
                if (single) {
                    let ans = { points: 0 };
                    for (let i = 0; i < result.length; ++i) {
                        ans[i] = result[i] ? true : false;
                        if (i < correct.length && result[i]) {
                            ans.points = 1;
                        }
                        if (i >= correct.length && result[i]) {
                            ans.points = -1;
                        }
                        scope.ans = ans;
                    }
                    if (ans.points == 1)
                        select(0);
                    else
                        select(1);
                }
                else {
                    let ans = { points: 0 };
                    for (let i = 0; i < result.length; ++i) {
                        ans[i] = result[i] ? true : false;
                        if (i < correct.length) {
                            if (result[i])
                                ans.points++;
                            else
                                ans.points--;
                        }
                        else {
                            if (result[i])
                                ans.points--;
                            else
                                ans.points++;
                        }
                    }
                    scope.ans = ans;
                    if (ans.points == choices.length)
                        select(0);
                    else
                        select(1);
                }
            });
            renderTarget.appendChild(submit);
        }
    },
    Custom: class extends GBNode_1.GBRoomNode {
        constructor() {
            super();
            this.AddAttribute("inputs", {
                label: GBLang_1.GBLang.tl("inputTypes"),
                type: "list",
                listType: "select",
                selectChoices: ["String", "Number", "Boolean"]
            });
            this.AddAttribute("labels", {
                label: GBLang_1.GBLang.tl("inputLabels"),
                type: "list",
                listType: "string"
            });
            this.AddAttribute("submit", {
                label: GBLang_1.GBLang.tl("submitText"),
                type: "string"
            });
            this.AddOutput("Output");
        }
        Render(select, scope, renderTarget) {
            renderTarget.innerHTML = GBRender_1.GBRender.GetRenderer().RenderRoom(this.GetAttribute("room"), scope);
            let inputs = this.GetAttribute("inputs");
            let labels = this.GetAttribute("labels");
            let ans = {};
            inputs.forEach((type, index) => {
                let input = document.createElement("input");
                if (type == "Number") {
                    input.type = "number";
                    input.value = "0";
                    ans[index] = 0;
                }
                else if (type == "Boolean") {
                    input.type = "checkbox";
                    ans[index] = false;
                }
                else {
                    input.type = "text";
                    ans[index] = "";
                }
                let label = document.createElement("label");
                if (labels[index] !== undefined) {
                    if (type == "Boolean") {
                        label.appendChild(input);
                        label.appendChild(document.createTextNode(" " + labels[index]));
                    }
                    else {
                        label.appendChild(document.createTextNode(labels[index] + ": "));
                        label.appendChild(input);
                    }
                }
                else {
                    label.appendChild(input);
                }
                input.addEventListener("change", () => {
                    if (type == "Boolean") {
                        ans[index] = input.checked;
                    }
                    else {
                        ans[index] = input.value;
                    }
                });
                renderTarget.appendChild(label);
            });
            let submit = document.createElement("input");
            submit.setAttribute("type", "button");
            submit.setAttribute("value", this.GetAttribute("submit"));
            submit.addEventListener("click", () => {
                scope.ans = ans;
                select(0);
                console.log(scope);
            });
            renderTarget.appendChild(submit);
        }
    },
    Math: class extends GBNode_1.GBToolNode {
        constructor() {
            super();
            this.AddAttribute("expression", {
                type: "string",
                label: GBLang_1.GBLang.tl("expression"),
                textarea: true
            });
            this.AddOutput("Output");
            this.SetAttribute("title", GBLang_1.GBLang.tl("math"), true);
        }
        Execute(scope) {
            scope["ans"] = GBMath_1.SecureEvaluate(this.GetAttribute("expression"), scope);
            return 0;
        }
    },
    Check: class extends GBNode_1.GBToolNode {
        constructor() {
            super();
            this.AddAttribute("expression", {
                type: "string",
                label: GBLang_1.GBLang.tl("expression")
            });
            this.AddOutput(GBLang_1.GBLang.tl("true"));
            this.AddOutput(GBLang_1.GBLang.tl("false"));
            this.SetAttribute("title", GBLang_1.GBLang.tl("Check"), true);
        }
        Execute(scope) {
            if (GBMath_1.SecureEvaluate(this.GetAttribute("expression"), scope)) {
                return 0;
            }
            else
                return 1;
        }
    },
    Relay: class extends GBNode_1.GBToolNode {
        constructor() {
            super();
            this.AddOutput("Output");
            this.SetAttributeProperty("title", "readonly", false);
            this.SetAttribute("title", GBLang_1.GBLang.tl("relay"));
        }
        Execute() {
            return 0;
        }
    },
    end: GBNode_1.GBEndNode
};
function ConstructNode(template) {
    let node = new GBNodeTemplates[template]();
    node.templateName = template;
    return node;
}
exports.ConstructNode = ConstructNode;
function GetTemplateNames() {
    return Object.keys(GBNodeTemplates);
}
exports.GetTemplateNames = GetTemplateNames;
// Helper functions
function ShuffleForEach(arr, callback) {
    let indices = new Array(arr.length);
    for (let i = 0; i < indices.length; ++i) {
        indices[i] = i;
    }
    ShuffleArray(indices);
    indices.forEach(index => {
        callback(arr[index], index);
    });
}
function ShuffleArray(arr) {
    for (var i = arr.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
//# sourceMappingURL=GBNodeTemplates.js.map