"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBNodeContainer = void 0;
const GBEndNode_1 = require("./Node/GBEndNode");
const GBRelayNode_1 = require("./Node/Templates/GBRelayNode");
const GBCheckNode_1 = require("./Node/Templates/GBCheckNode");
const GBMathNode_1 = require("./Node/Templates/GBMathNode");
const GBCustomNode_1 = require("./Node/Templates/GBCustomNode");
const GBMultipleChoiceNode_1 = require("./Node/Templates/GBMultipleChoiceNode");
const GBDecisionNode_1 = require("./Node/Templates/GBDecisionNode");
/**
 * Node templates are defined here. To add a new template, just add a new node instance to the object,
 * define its attributes in its constructor after calling the super constructor and then implement
 * all inherited abstract methods.
 *
 * GBRoomNode needs a Render method which gets a select callback, a scope and a renderTarget.
 * Render everything you want the player to see to the rendertarget and bind the select callback with
 * the wanted output index to an input event. It is advised to first render the room description.
 *
 * GBToolNode needs an Execute method which gets a scope and returns a number. The scope contains all
 * variables in the current gamebook. Calculate what you want and return the output index that should be
 * chosen.
 *
 * Look at existing templates as examples.
 */
class GBNodeContainer {
    static constructNode(template) {
        const node = new GBNodeContainer.TEMPLATES[template]();
        node.templateName = template;
        return node;
    }
    static setTemplate(name, classObject) {
        GBNodeContainer.TEMPLATES[name] = classObject;
    }
    static getTemplateNames() {
        return Object.keys(GBNodeContainer.TEMPLATES);
    }
}
exports.GBNodeContainer = GBNodeContainer;
GBNodeContainer.TEMPLATES = {
    Decision: GBDecisionNode_1.GBDecisionNode,
    "Multiple choice": GBMultipleChoiceNode_1.GBMultipleChoiceNode,
    Custom: GBCustomNode_1.GBCustomNode,
    Math: GBMathNode_1.GBMathNode,
    Check: GBCheckNode_1.GBCheckNode,
    Relay: GBRelayNode_1.GBRelayNode,
    end: GBEndNode_1.GBEndNode
};
//# sourceMappingURL=GBNodeContainer.js.map