/**
 * Structure to serialize gamebooks
 */
export interface GBSave {
    n: {
        t: string;
        a: string;
    }[];
    nc: number;
    i: string[];
    ic: number;
}
