"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SecureEvaluate = void 0;
const mathjs_1 = require("mathjs");
const math = mathjs_1.create(mathjs_1.all, {});
const limitedEvaluate = math.evaluate;
math.import({
    import: function () {
        throw new Error("Function import is disabled");
    },
    createUnit: function () {
        throw new Error("Function createUnit is disabled");
    },
    evaluate: function () {
        throw new Error("Function evaluate is disabled");
    },
    parse: function () {
        throw new Error("Function parse is disabled");
    },
    simplify: function () {
        throw new Error("Function simplify is disabled");
    },
    derivative: function () {
        throw new Error("Function derivative is disabled");
    }
}, { override: true });
/**
 * Uses mathjs with secure configuration to evaluate an expression with a given scope
 * @param expr Expression to evaluate
 * @param scope Object containing context
 */
function SecureEvaluate(expr, scope) {
    return limitedEvaluate(expr, scope);
}
exports.SecureEvaluate = SecureEvaluate;
//# sourceMappingURL=GBMath.js.map