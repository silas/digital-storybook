import { GBMediaNode } from "./Node/GBMediaNode";
export declare class GBRender {
    private static instance;
    private media;
    private prefix;
    private audioPlaybackSpeedGetter;
    constructor();
    /**
     * Get singleton instance of renderer
     * @returns instance
     */
    static GetRenderer(): GBRender;
    setPlaybackSpeedGetter(getter: () => number): void;
    /**
     * Substitute math expressions in a string with a given scope
     * @param text String to substitute math in
     * @param scope Context for math evaluation
     * @returns Processed string
     */
    SubstituteMath(text: string, scope: {
        [key: string]: any;
    }): string;
    /**
     * Set media array
     * @param media media array
     */
    SetMedia(media: {
        [tag: string]: GBMediaNode;
    }): void;
    /**
     * Substitutes media tags with corresponding HTML
     * @param text String to substitute media tags in
     * @returns Processed string
     */
    InsertMedia(text: string): string;
    private generateVideo;
    private generateAudio;
    /**
     * Generate HTML from media node
     * @param media Media node
     * @returns HTML string
     */
    private GenerateMedia;
    /**
     * Set the prefix which gets prepended
     * @param prefix Prefix
     */
    SetPrefix(prefix: string): void;
    /**
     * Prepends the configured prefix to a string
     * @param text String
     * @returns Processed string
     */
    PrependPrefix(text: string): string;
    /**
     * Process markdown in a given string
     * @param text String to process
     * @returns Processed string
     */
    RenderMarkdown(text: string): string;
    /**
     * Apply the whole render-pipeline to a string.
     * First prepends the prefix, then substitutes math expressions,
     * then substitutes media tags, then parses markdown and finally
     * purifies the DOM
     * @param text String to process
     * @param scope Context for math expressions
     * @returns HTMLElement
     */
    RenderRoom(text: string, scope: {
        [key: string]: any;
    }): HTMLElement;
}
