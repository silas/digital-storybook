"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBMenuItem = exports.GBMenu = exports.GBMenubar = void 0;
class GBMenubar {
    constructor(container, style = "") {
        this._open = false;
        this.style = style;
        this.menubar = document.createElement("div");
        this.menubar.setAttribute("class", this.style);
        this._menu = new GBMenu(this.menubar);
        container.appendChild(this.menubar);
    }
    Open() {
        this.menubar.setAttribute("class", this.style + " open");
        this._open = true;
    }
    Close() {
        this.menubar.setAttribute("class", this.style);
        this._open = false;
    }
    Toggle() {
        if (this.open)
            this.Close();
        else
            this.Open();
    }
    get open() {
        return this._open;
    }
    get menu() {
        return this._menu;
    }
}
exports.GBMenubar = GBMenubar;
class GBMenu {
    constructor(container, id) {
        this.id = "";
        this._open = true;
        this.menuItems = [];
        this.subMenus = [];
        this._container = container;
        this.id = id;
        this.menuContainer = document.createElement("div");
        this.menu = document.createElement("ul");
        this.menuContainer.appendChild(this.menu);
        this._container.appendChild(this.menuContainer);
    }
    /**
     * Add a new menu item
     * @param text Label of item
     * @param callback Method to call on click
     * @returns Menu item
     */
    AddMenuItem(text = "", callback) {
        let item = new GBMenuItem(this.menu, text, callback);
        this.menuItems.push(item);
        return item;
    }
    /**
     * Remove a menu item
     * @param item item to remove
     */
    RemoveMenuItem(item) {
        item.Remove();
        this.menuItems = this.menuItems.filter(i => i !== item);
    }
    /**
     * Get an array containing all menu items
     * @returns Array containing all menu items
     */
    GetMenuItems() {
        return this.menuItems;
    }
    /**
     * Add a new submenu
     * @param text Label for the submenu
     * @param id
     * @returns new submenu
     */
    AddSubMenu(text, id) {
        let subcontainer = document.createElement("div");
        new GBMenuItem(subcontainer, text + " >", () => {
            submenu.Toggle();
        });
        let submenu = new GBMenu(subcontainer, id);
        submenu.Close();
        this.menu.appendChild(subcontainer);
        this.subMenus.push(submenu);
        return submenu;
    }
    /**
     * Remove a submenu
     * @param menu submenu to remove
     */
    RemoveSubMenu(menu) {
        this.menu.removeChild(menu.container);
        this.subMenus = this.subMenus.filter(m => m !== menu);
    }
    /**
     * Get an array containing all sub menus
     * @returns Array containing all sub menus
     */
    GetSubMenus() {
        return this.subMenus;
    }
    /**
     * Open the menu
     */
    Open() {
        this.menuContainer.setAttribute("class", "");
        this._open = true;
    }
    /**
     * Close the menu
     */
    Close() {
        this.menuContainer.setAttribute("class", "hidden");
        this._open = false;
    }
    /**
     * Toggle the menu
     */
    Toggle() {
        if (this.open)
            this.Close();
        else
            this.Open();
    }
    /// Accessors
    get open() {
        return this._open;
    }
    get container() {
        return this._container;
    }
}
exports.GBMenu = GBMenu;
class GBMenuItem {
    constructor(container, text = "", callback) {
        this.callback = function () { };
        this.container = container;
        this.item = document.createElement("li");
        this.anchor = document.createElement("a");
        this.anchor.setAttribute("tabindex", "0");
        this.text = text;
        if (callback)
            this.SetCallback(callback);
        this.item.appendChild(this.anchor);
        this.container.appendChild(this.item);
    }
    /**
     * Set the callback method of the menu item
     * @param callback Callback method
     */
    SetCallback(callback) {
        this.anchor.removeEventListener("click", this.callback);
        this.callback = (e) => {
            e.preventDefault();
            e.stopPropagation();
            callback(e);
        };
        this.anchor.addEventListener("click", this.callback);
    }
    /**
     * Remove menu item from menu
     */
    Remove() {
        this.container.removeChild(this.item);
    }
    /// Accessors
    set text(text) {
        this.anchor.textContent = text;
    }
    get text() {
        return this.anchor.textContent ? this.anchor.textContent : "";
    }
}
exports.GBMenuItem = GBMenuItem;
//# sourceMappingURL=GBMenu.js.map