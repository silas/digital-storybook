import { GBEditorNode } from "./Node/GBEditorNode";
/**
 * Node templates are defined here. To add a new template, just add a new node instance to the object,
 * define its attributes in its constructor after calling the super constructor and then implement
 * all inherited abstract methods.
 *
 * GBRoomNode needs a Render method which gets a select callback, a scope and a renderTarget.
 * Render everything you want the player to see to the rendertarget and bind the select callback with
 * the wanted output index to an input event. It is advised to first render the room description.
 *
 * GBToolNode needs an Execute method which gets a scope and returns a number. The scope contains all
 * variables in the current gamebook. Calculate what you want and return the output index that should be
 * chosen.
 *
 * Look at existing templates as examples.
 */
export declare class GBNodeContainer {
    private static TEMPLATES;
    static constructNode(template: string): GBEditorNode;
    static setTemplate(name: any, classObject: any): void;
    static getTemplateNames(): string[];
}
