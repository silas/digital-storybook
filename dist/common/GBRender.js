"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBRender = void 0;
const GBLang_1 = require("../language/GBLang");
const marked = require("marked");
const DOMPurify = require("dompurify");
const GBMath_1 = require("../common/GBMath");
class GBRender {
    constructor() {
        this.media = {};
        this.prefix = "";
        this.audioPlaybackSpeedGetter = () => 1;
        marked.setOptions({});
    }
    /**
     * Get singleton instance of renderer
     * @returns instance
     */
    static GetRenderer() {
        if (!GBRender.instance) {
            GBRender.instance = new GBRender();
        }
        return GBRender.instance;
    }
    setPlaybackSpeedGetter(getter) {
        if (getter !== null) {
            this.audioPlaybackSpeedGetter = getter;
        }
    }
    /**
     * Substitute math expressions in a string with a given scope
     * @param text String to substitute math in
     * @param scope Context for math evaluation
     * @returns Processed string
     */
    SubstituteMath(text, scope) {
        return text.replace(/\$([^\!][^\$]*)\$/g, (_, expr) => {
            try {
                return GBMath_1.SecureEvaluate(expr, scope);
            }
            catch (error) {
                return "{Error: " + error + "}";
            }
        });
    }
    /**
     * Set media array
     * @param media media array
     */
    SetMedia(media) {
        this.media = media;
    }
    /**
     * Substitutes media tags with corresponding HTML
     * @param text String to substitute media tags in
     * @returns Processed string
     */
    InsertMedia(text) {
        return text.replace(/(?:^|[^\$])\$!([a-zA-Z_]\w*)/g, (m, v) => {
            if (this.media[v] !== undefined) {
                let image = this.media[v];
                return this.GenerateMedia(image);
            }
            return GBLang_1.GBLang.tl("mediaNotFound", true);
        });
    }
    generateVideo(media) {
        let classes = "";
        if (media.GetAttribute("stretch"))
            classes += "stretch ";
        if (media.GetAttribute("center"))
            classes += "center ";
        if (media.GetAttribute("inline"))
            classes += "inline ";
        const figureElement = document.createElement("figure");
        figureElement.setAttribute("class", classes);
        figureElement.classList.add("gb-video-figure");
        let videoElement = document.createElement("video");
        videoElement.src = media.GetAttribute("url");
        videoElement.controls = false;
        videoElement.appendChild(GBLang_1.GBLang.tl("video"));
        videoElement.setAttribute("class", classes);
        const subtitles = media.GetAttribute("subtitles");
        if (subtitles) {
            const subtitleTrack = document.createElement("track");
            subtitleTrack.label = GBLang_1.GBLang.tl("subtitles", true);
            subtitleTrack.kind = "subtitles";
            subtitleTrack.srclang = "de";
            subtitleTrack.src = subtitles;
            videoElement.appendChild(subtitleTrack);
        }
        figureElement.appendChild(videoElement);
        const controlElement = document.createElement("ul");
        controlElement.classList.add("gb-video-controls");
        figureElement.appendChild(controlElement);
        const pauseButton = document.createElement("button");
        pauseButton.classList.add("gb-video-control-pause");
        pauseButton.innerHTML = "&#9654;";
        const pauseContainer = document.createElement("li");
        pauseContainer.appendChild(pauseButton);
        const progressElement = document.createElement("input");
        progressElement.classList.add("gb-video-control-progress");
        progressElement.type = "range";
        progressElement.value = "0";
        progressElement.setAttribute("min", "0");
        const progressContainer = document.createElement("li");
        progressContainer.appendChild(progressElement);
        progressContainer.style.flex = "1";
        const volumeButton = document.createElement("button");
        volumeButton.classList.add("gb-video-control-volume");
        const volumeSign = document.createElement("span");
        volumeSign.innerHTML = "&#128265;";
        volumeButton.appendChild(volumeSign);
        const volumeContainer = document.createElement("li");
        volumeContainer.appendChild(volumeButton);
        const volumeSlider = document.createElement("input");
        volumeSlider.classList.add("gb-video-control-volume-slider");
        volumeSlider.type = "range";
        volumeSlider.setAttribute("min", "0");
        volumeSlider.setAttribute("max", "100");
        volumeSlider.setAttribute("value", "100");
        volumeButton.appendChild(volumeSlider);
        const fullscreenButton = document.createElement("button");
        fullscreenButton.classList.add("gb-video-control-fullscreen");
        fullscreenButton.innerHTML = "&#9974;";
        const fullscreenContainer = document.createElement("li");
        fullscreenContainer.appendChild(fullscreenButton);
        let container = document.createElement("span");
        const speedSelect = document.createElement("select");
        container.classList.add("video-node");
        const speeds = ["0,5", "0,75", "1,0", "1,5", "2,0"];
        speeds.forEach(speed => {
            const value = speed.replace(",", ".");
            const option = document.createElement("option");
            option.value = value;
            option.innerText = speed;
            if (speed === "1,0") {
                option.setAttribute("selected", "selected");
            }
            speedSelect.appendChild(option);
        });
        const speedContainer = document.createElement("li");
        speedContainer.appendChild(speedSelect);
        let subtitleContainer = null;
        if (subtitles) {
            const subtitleButton = document.createElement("button");
            subtitleButton.classList.add("gb-video-control-subtitle");
            subtitleButton.innerText = "CC";
            subtitleContainer = document.createElement("li");
            subtitleContainer.appendChild(subtitleButton);
        }
        controlElement.appendChild(pauseContainer);
        controlElement.appendChild(progressContainer);
        controlElement.appendChild(volumeContainer);
        if (subtitleContainer) {
            controlElement.appendChild(subtitleContainer);
        }
        controlElement.appendChild(speedContainer);
        controlElement.appendChild(fullscreenContainer);
        container.appendChild(figureElement);
        const altData = media.GetAttribute("alt");
        if (altData) {
            let transcript = document.createElement("p");
            transcript.appendChild(GBLang_1.GBLang.tl("transcript"));
            transcript.appendChild(document.createTextNode(": " + altData));
            transcript.setAttribute("style", "color:darkblue");
            container.appendChild(transcript);
        }
        return container.outerHTML;
    }
    generateAudio(media) {
        let classes = "";
        if (media.GetAttribute("stretch"))
            classes += "stretch ";
        if (media.GetAttribute("center"))
            classes += "center ";
        if (media.GetAttribute("inline"))
            classes += "inline ";
        let audio = document.createElement("audio");
        audio.src = media.GetAttribute("url");
        audio.controls = true;
        audio.appendChild(GBLang_1.GBLang.tl("audio"));
        audio.setAttribute("class", classes);
        let container = document.createElement("div");
        container.classList.add("gb-audio");
        container.appendChild(audio);
        const altData = media.GetAttribute("alt");
        if (altData) {
            let transcript = document.createElement("p");
            transcript.appendChild(GBLang_1.GBLang.tl("transcript"));
            transcript.appendChild(document.createTextNode(": " + altData));
            transcript.setAttribute("style", "color:darkblue");
            container.appendChild(transcript);
        }
        return container.outerHTML;
    }
    /**
     * Generate HTML from media node
     * @param media Media node
     * @returns HTML string
     */
    GenerateMedia(media) {
        let type = media.GetAttribute("media");
        let classes = "";
        if (media.GetAttribute("stretch"))
            classes += "stretch ";
        if (media.GetAttribute("center"))
            classes += "center ";
        if (media.GetAttribute("inline"))
            classes += "inline ";
        if (type == "Video") {
            return this.generateVideo(media);
        }
        else if (type == "Audio") {
            return this.generateAudio(media);
        }
        else {
            let image = document.createElement("img");
            image.src = media.GetAttribute("url");
            image.alt = media.GetAttribute("alt");
            image.setAttribute("class", classes);
            return image.outerHTML;
        }
    }
    /**
     * Set the prefix which gets prepended
     * @param prefix Prefix
     */
    SetPrefix(prefix) {
        this.prefix = prefix;
    }
    /**
     * Prepends the configured prefix to a string
     * @param text String
     * @returns Processed string
     */
    PrependPrefix(text) {
        return this.prefix + "\n\n" + text;
    }
    /**
     * Process markdown in a given string
     * @param text String to process
     * @returns Processed string
     */
    RenderMarkdown(text) {
        return marked(text);
    }
    /**
     * Apply the whole render-pipeline to a string.
     * First prepends the prefix, then substitutes math expressions,
     * then substitutes media tags, then parses markdown and finally
     * purifies the DOM
     * @param text String to process
     * @param scope Context for math expressions
     * @returns HTMLElement
     */
    RenderRoom(text, scope) {
        const innerHTML = DOMPurify.sanitize(this.RenderMarkdown(this.InsertMedia(this.SubstituteMath(this.PrependPrefix(text), scope))), { ALLOW_UNKNOWN_PROTOCOLS: true });
        const room = document.createElement("span");
        room.classList.add("GBRoom");
        room.innerHTML = innerHTML;
        room.querySelectorAll(".gb-video-figure").forEach(videoNode => {
            const video = videoNode.querySelector("video");
            const select = videoNode.querySelector("select");
            // const controls = video.querySelector(".gb-video-control")
            const pauseButton = videoNode.querySelector(".gb-video-control-pause");
            const volumeButton = videoNode.querySelector(".gb-video-control-volume");
            const volumeSlider = videoNode.querySelector(".gb-video-control-volume-slider");
            const progressElement = videoNode.querySelector(".gb-video-control-progress");
            const fullscreenButton = videoNode.querySelector(".gb-video-control-fullscreen");
            const subtitleButton = videoNode.querySelector(".gb-video-control-subtitle");
            const volumeSign = volumeButton.querySelector("span");
            pauseButton.addEventListener("click", () => {
                if (video.paused || video.ended) {
                    video.play();
                    pauseButton.innerHTML = "&#9208;";
                }
                else {
                    video.pause();
                    pauseButton.innerHTML = "&#9654;";
                }
            });
            volumeButton.addEventListener("click", () => {
                video.muted = !video.muted;
                if (!video.muted && video.volume > 0) {
                    volumeSign.innerHTML = "&#128265;";
                }
                else {
                    volumeSign.innerHTML = "&#128263;";
                }
            });
            volumeButton.addEventListener("touchstart", (e) => {
                // @ts-ignore
                volumeSlider.focus();
                e.stopPropagation();
            });
            function updateVolume() {
                // @ts-ignore
                video.volume = Math.min(1, Math.max(0, parseInt(volumeSlider.value) / 100));
                if (video.volume > 0) {
                    volumeSign.innerHTML = "&#128265;";
                    video.muted = false;
                }
                else {
                    volumeSign.innerHTML = "&#128263;";
                }
            }
            volumeSlider.addEventListener("click", (e) => {
                updateVolume();
                e.cancelBubble = true;
            });
            volumeSlider.addEventListener("touchend", (e) => {
                updateVolume();
                e.cancelBubble = true;
            });
            progressElement.addEventListener("click", () => {
                // @ts-ignore
                video.currentTime = parseInt(progressElement.value);
            });
            progressElement.addEventListener("touchend", () => {
                // @ts-ignore
                video.currentTime = parseInt(progressElement.value);
            });
            fullscreenButton.addEventListener("click", () => {
                if (!document.fullscreenElement) {
                    videoNode.requestFullscreen();
                }
                else {
                    document.exitFullscreen();
                }
            });
            video.addEventListener("loadedmetadata", () => {
                progressElement.setAttribute("max", video.duration + "");
                progressElement.setAttribute("value", "0");
            });
            video.addEventListener("timeupdate", () => {
                if (!progressElement.getAttribute("max")) {
                    progressElement.setAttribute("max", video.duration + "");
                }
                progressElement.setAttribute("value", video.currentTime + "");
            });
            select.addEventListener("change", () => {
                video.playbackRate = parseFloat(select.value);
            });
            if (subtitleButton) {
                subtitleButton.addEventListener("click", () => {
                    const track = video.textTracks[0];
                    if (track) {
                        if (track.mode === "showing") {
                            track.mode = "hidden";
                        }
                        else {
                            track.mode = "showing";
                        }
                    }
                });
            }
        });
        room.querySelectorAll(".gb-audio audio").forEach(audioNode => {
            audioNode.addEventListener("timeupdate", () => {
                audioNode.playbackRate = this.audioPlaybackSpeedGetter();
            });
        });
        return room;
    }
}
exports.GBRender = GBRender;
//# sourceMappingURL=GBRender.js.map
