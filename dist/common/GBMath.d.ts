/**
 * Uses mathjs with secure configuration to evaluate an expression with a given scope
 * @param expr Expression to evaluate
 * @param scope Object containing context
 */
export declare function SecureEvaluate(expr: string, scope?: object | undefined): any;
