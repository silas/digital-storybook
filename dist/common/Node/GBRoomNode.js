"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBRoomNode = void 0;
// Parent class of room nodes
const GBLang_1 = require("../../language/GBLang");
const GBEditorNode_1 = require("./GBEditorNode");
class GBRoomNode extends GBEditorNode_1.GBEditorNode {
    constructor() {
        super();
        this.AddAttribute("room", {
            label: GBLang_1.GBLang.tl("room", true),
            type: "string",
            textarea: true
        });
    }
}
exports.GBRoomNode = GBRoomNode;
//# sourceMappingURL=GBRoomNode.js.map