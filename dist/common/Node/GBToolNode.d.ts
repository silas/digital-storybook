import { GBEditorNode } from "./GBEditorNode";
export declare abstract class GBToolNode extends GBEditorNode {
    constructor();
    /**
     * Executes toolnode. select function must be called with a valid output id.
     * @param scope Variable register.
     * @returns Selected output.
     */
    abstract Execute(scope: {
        [key: string]: any;
    }): number;
}
