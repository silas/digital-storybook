"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBToolNode = void 0;
// Parent class of tool nodes
const GBEditorNode_1 = require("./GBEditorNode");
class GBToolNode extends GBEditorNode_1.GBEditorNode {
    constructor() {
        super();
        this.SetAttributeProperty("title", "readonly", true);
        this.SetAttribute("color", "darkgreen");
    }
}
exports.GBToolNode = GBToolNode;
//# sourceMappingURL=GBToolNode.js.map