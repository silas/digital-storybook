import { GBToolNode } from "../GBToolNode";
export declare class GBMathNode extends GBToolNode {
    constructor();
    Execute(scope: {
        [key: string]: any;
    }): number;
}
