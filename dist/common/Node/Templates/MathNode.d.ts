import { GBToolNode } from "../GBToolNode";
export declare class MathNode extends GBToolNode {
    constructor();
    Execute(scope: {
        [key: string]: any;
    }): number;
}
