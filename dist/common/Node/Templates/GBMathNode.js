"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBMathNode = void 0;
const GBToolNode_1 = require("../GBToolNode");
const GBLang_1 = require("../../../language/GBLang");
const GBMath_1 = require("../../GBMath");
class GBMathNode extends GBToolNode_1.GBToolNode {
    constructor() {
        super();
        this.AddAttribute("expression", {
            type: "string",
            label: GBLang_1.GBLang.tl("expression", true),
            textarea: true
        });
        this.AddOutput("Output");
        this.SetAttribute("title", GBLang_1.GBLang.tl("math", true), true);
    }
    Execute(scope) {
        scope["ans"] = GBMath_1.SecureEvaluate(this.GetAttribute("expression"), scope);
        return 0;
    }
}
exports.GBMathNode = GBMathNode;
//# sourceMappingURL=GBMathNode.js.map