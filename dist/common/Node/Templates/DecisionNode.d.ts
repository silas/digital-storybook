import { GBRoomNode } from "../GBRoomNode";
export declare class DecisionNode extends GBRoomNode {
    constructor();
    Update(decisions: string[]): void;
    Render(select: (output: number) => void, scope: {
        [key: string]: any;
    }, renderTarget: HTMLElement): void;
}
