import { GBToolNode } from "../GBToolNode";
export declare class GBCheckNode extends GBToolNode {
    constructor();
    Execute(scope: {
        [key: string]: any;
    }): number;
}
