import { GBToolNode } from "../GBToolNode";
export declare class CheckNode extends GBToolNode {
    constructor();
    Execute(scope: {
        [key: string]: any;
    }): number;
}
