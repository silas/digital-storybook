"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBRelayNode = void 0;
const GBToolNode_1 = require("../GBToolNode");
const GBLang_1 = require("../../../language/GBLang");
class GBRelayNode extends GBToolNode_1.GBToolNode {
    constructor() {
        super();
        this.AddOutput("Output");
        this.SetAttributeProperty("title", "readonly", false);
        this.SetAttribute("title", GBLang_1.GBLang.tl("relay", true));
    }
    Execute() {
        return 0;
    }
}
exports.GBRelayNode = GBRelayNode;
//# sourceMappingURL=GBRelayNode.js.map