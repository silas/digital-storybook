"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MultipleChoiceNode = void 0;
const GBRoomNode_1 = require("../GBRoomNode");
const GBLang_1 = require("../../../language/GBLang");
const GBRender_1 = require("../../GBRender");
const GBNode_1 = require("../GBNode");
class MultipleChoiceNode extends GBRoomNode_1.GBRoomNode {
    constructor() {
        super();
        this.AddAttribute("true", {
            label: GBLang_1.GBLang.tl("right"),
            type: "list",
            listType: "string",
            textarea: true
        });
        this.AddAttribute("false", {
            label: GBLang_1.GBLang.tl("wrong"),
            type: "list",
            listType: "string",
            textarea: true
        });
        this.AddAttribute("single", {
            label: GBLang_1.GBLang.tl("single"),
            type: "boolean"
        });
        this.AddAttribute("submit", {
            label: GBLang_1.GBLang.tl("submitText"),
            type: "string"
        }, GBLang_1.GBLang.tl("submit"));
        this.AddOutput(GBLang_1.GBLang.tl("correct"));
        this.AddOutput(GBLang_1.GBLang.tl("incorrect"));
    }
    Render(select, scope, renderTarget) {
        renderTarget.innerHTML = GBRender_1.GBRender.GetRenderer().RenderRoom(this.GetAttribute("room"), scope);
        let correct = this.GetAttribute("true");
        let incorrect = this.GetAttribute("false");
        let choices = correct.concat(incorrect);
        let result = new Array(choices.length);
        let single = this.GetAttribute("single");
        GBNode_1.GBNode.ShuffleForEach(choices, (choice, index) => {
            let input = document.createElement("input");
            input.id = "mc" + index;
            if (single) {
                input.setAttribute("type", "radio");
                input.setAttribute("name", "mc");
            }
            else {
                input.setAttribute("type", "checkbox");
            }
            let label = document.createElement("label");
            label.setAttribute("for", input.id);
            input.addEventListener("change", () => {
                result[index] = input.checked;
            });
            label.appendChild(input);
            label.appendChild(document.createTextNode(GBRender_1.GBRender.GetRenderer().SubstituteMath(choice, scope)));
            renderTarget.appendChild(label);
        });
        let submit = document.createElement("input");
        submit.setAttribute("type", "button");
        submit.setAttribute("value", this.GetAttribute("submit"));
        submit.addEventListener("click", () => {
            if (single) {
                let ans = { points: 0 };
                for (let i = 0; i < result.length; ++i) {
                    ans[i] = result[i] ? true : false;
                    if (i < correct.length && result[i]) {
                        ans.points = 1;
                    }
                    if (i >= correct.length && result[i]) {
                        ans.points = -1;
                    }
                    scope.ans = ans;
                }
                if (ans.points == 1)
                    select(0);
                else
                    select(1);
            }
            else {
                let ans = { points: 0 };
                for (let i = 0; i < result.length; ++i) {
                    ans[i] = result[i] ? true : false;
                    if (i < correct.length) {
                        if (result[i])
                            ans.points++;
                        else
                            ans.points--;
                    }
                    else {
                        if (result[i])
                            ans.points--;
                        else
                            ans.points++;
                    }
                }
                scope.ans = ans;
                if (ans.points == choices.length)
                    select(0);
                else
                    select(1);
            }
        });
        renderTarget.appendChild(submit);
    }
}
exports.MultipleChoiceNode = MultipleChoiceNode;
//# sourceMappingURL=MultipleChoiceNode.js.map