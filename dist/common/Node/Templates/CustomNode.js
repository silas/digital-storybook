"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomNode = void 0;
const GBRoomNode_1 = require("../GBRoomNode");
const GBLang_1 = require("../../../language/GBLang");
const GBRender_1 = require("../../GBRender");
class CustomNode extends GBRoomNode_1.GBRoomNode {
    constructor() {
        super();
        this.AddAttribute("inputs", {
            label: GBLang_1.GBLang.tl("inputTypes"),
            type: "list",
            listType: "select",
            selectChoices: ["String", "Number", "Boolean"]
        });
        this.AddAttribute("labels", {
            label: GBLang_1.GBLang.tl("inputLabels"),
            type: "list",
            listType: "string"
        });
        this.AddAttribute("submit", {
            label: GBLang_1.GBLang.tl("submitText"),
            type: "string"
        });
        this.AddOutput("Output");
    }
    Render(select, scope, renderTarget) {
        renderTarget.innerHTML = GBRender_1.GBRender.GetRenderer().RenderRoom(this.GetAttribute("room"), scope);
        let inputs = this.GetAttribute("inputs");
        let labels = this.GetAttribute("labels");
        let ans = {};
        inputs.forEach((type, index) => {
            let input = document.createElement("input");
            if (type == "Number") {
                input.type = "number";
                input.value = "0";
                ans[index] = 0;
            }
            else if (type == "Boolean") {
                input.type = "checkbox";
                ans[index] = false;
            }
            else {
                input.type = "text";
                ans[index] = "";
            }
            let label = document.createElement("label");
            if (labels[index] !== undefined) {
                if (type == "Boolean") {
                    label.appendChild(input);
                    label.appendChild(document.createTextNode(" " + labels[index]));
                }
                else {
                    label.appendChild(document.createTextNode(labels[index] + ": "));
                    label.appendChild(input);
                }
            }
            else {
                label.appendChild(input);
            }
            input.addEventListener("change", () => {
                if (type == "Boolean") {
                    ans[index] = input.checked;
                }
                else {
                    ans[index] = input.value;
                }
            });
            renderTarget.appendChild(label);
        });
        let submit = document.createElement("input");
        submit.setAttribute("type", "button");
        submit.setAttribute("value", this.GetAttribute("submit"));
        submit.addEventListener("click", () => {
            scope.ans = ans;
            select(0);
            console.log(scope);
        });
        renderTarget.appendChild(submit);
    }
}
exports.CustomNode = CustomNode;
//# sourceMappingURL=CustomNode.js.map