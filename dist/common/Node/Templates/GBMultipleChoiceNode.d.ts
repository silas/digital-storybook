import { GBRoomNode } from "../GBRoomNode";
export declare class GBMultipleChoiceNode extends GBRoomNode {
    constructor();
    Render(select: (output: number) => void, scope: {
        [key: string]: any;
    }, renderTarget: HTMLElement): void;
}
