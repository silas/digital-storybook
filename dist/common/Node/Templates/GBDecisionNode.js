"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBDecisionNode = void 0;
const GBRoomNode_1 = require("../GBRoomNode");
const GBLang_1 = require("../../../language/GBLang");
const GBRender_1 = require("../../GBRender");
const GBNode_1 = require("../GBNode");
class GBDecisionNode extends GBRoomNode_1.GBRoomNode {
    constructor() {
        super();
        this.AddAttribute("decisions", {
            label: GBLang_1.GBLang.tl("decisions", true),
            type: "list",
            listType: "string",
            textarea: true
        });
        this.AddAttribute("shuffle", {
            type: "boolean",
            label: GBLang_1.GBLang.tl("shuffle", true)
        });
        this.On("decisions", decisions => this.Update(decisions));
    }
    Update(decisions) {
        let diff = decisions.length - this.ListAttributeLength("outputTargets");
        if (diff > 0) {
            while (diff-- > 0) {
                this.AddOutput();
            }
        }
        else {
            while (diff++ < 0) {
                this.RemoveLastOutput();
            }
        }
        this.SetAttribute("outputTitles", this.GetAttribute("decisions"));
        this.Trigger("outputTitles", decisions, false);
    }
    Render(select, scope, renderTarget) {
        renderTarget.innerText = "";
        renderTarget.appendChild(GBRender_1.GBRender.GetRenderer().RenderRoom(this.GetAttribute("room"), scope));
        let decisions = this.GetAttribute("decisions");
        let _createButton = (decision, index) => {
            let button = document.createElement("input");
            button.setAttribute("type", "button");
            button.setAttribute("value", GBRender_1.GBRender.GetRenderer().SubstituteMath(decision, scope));
            button.addEventListener("click", () => {
                button.setAttribute("style", "background-color: lightblue");
                button.value = GBLang_1.GBLang.tl("pressed", true) + ": " + button.value;
                select(index);
            });
            renderTarget.appendChild(button);
        };
        if (this.GetAttribute("shuffle")) {
            GBNode_1.GBNode.ShuffleForEach(decisions, _createButton);
        }
        else {
            decisions.forEach(_createButton);
        }
    }
}
exports.GBDecisionNode = GBDecisionNode;
//# sourceMappingURL=GBDecisionNode.js.map