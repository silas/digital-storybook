"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckNode = void 0;
const GBToolNode_1 = require("../GBToolNode");
const GBLang_1 = require("../../../language/GBLang");
const GBMath_1 = require("../../GBMath");
class CheckNode extends GBToolNode_1.GBToolNode {
    constructor() {
        super();
        this.AddAttribute("expression", {
            type: "string",
            label: GBLang_1.GBLang.tl("expression")
        });
        this.AddOutput(GBLang_1.GBLang.tl("true"));
        this.AddOutput(GBLang_1.GBLang.tl("false"));
        this.SetAttribute("title", GBLang_1.GBLang.tl("Check"), true);
    }
    Execute(scope) {
        if (GBMath_1.SecureEvaluate(this.GetAttribute("expression"), scope)) {
            return 0;
        }
        else
            return 1;
    }
}
exports.CheckNode = CheckNode;
//# sourceMappingURL=CheckNode.js.map