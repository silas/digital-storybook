import { GBRoomNode } from "../GBRoomNode";
export declare class GBCustomNode extends GBRoomNode {
    constructor();
    Render(select: (output: number) => void, scope: {
        [key: string]: any;
    }, renderTarget: HTMLElement): void;
}
