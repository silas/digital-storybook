"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBVideoNode = void 0;
const GBMediaNode_1 = require("../GBMediaNode");
class GBVideoNode extends GBMediaNode_1.GBMediaNode {
    constructor() {
        super();
        this.SetAttribute("media", "Video");
        this.AddAttribute("url", {
            type: "file",
            label: "File",
            "video": true,
            "name": "select Video"
        });
        this.AddAttribute("subtitles", {
            type: "file",
            label: "File",
            "accept": ".vtt",
            "name": "select Subtitles"
        });
    }
}
exports.GBVideoNode = GBVideoNode;
//# sourceMappingURL=GBVideoNode.js.map