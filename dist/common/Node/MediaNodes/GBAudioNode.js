"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBAudioNode = void 0;
const GBMediaNode_1 = require("../GBMediaNode");
class GBAudioNode extends GBMediaNode_1.GBMediaNode {
    constructor() {
        super();
        this.SetAttribute("media", "Audio");
        this.AddAttribute("url", {
            type: "file",
            label: "File",
            "audio": true,
            "name": "select Audio",
        });
    }
}
exports.GBAudioNode = GBAudioNode;
//# sourceMappingURL=GBAudioNode.js.map