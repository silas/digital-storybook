"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBImageNode = void 0;
const GBMediaNode_1 = require("../GBMediaNode");
class GBImageNode extends GBMediaNode_1.GBMediaNode {
    constructor() {
        super();
        this.SetAttribute("media", "Image");
        this.AddAttribute("url", {
            type: "file",
            label: "File",
            "image": true,
            "name": "select Image"
        });
    }
}
exports.GBImageNode = GBImageNode;
//# sourceMappingURL=GBImageNode.js.map