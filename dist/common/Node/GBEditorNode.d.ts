import { GBNode } from "./GBNode";
export declare abstract class GBEditorNode extends GBNode {
    constructor();
    /**
     * Adds an output to the node.
     * @param title Title of the added output.
     */
    AddOutput(title?: string): void;
    /**
     * Removes an output from the node.
     * @param index Index of the output.
     */
    RemoveOutput(index: number): void;
    /**
     * Removes the last output.
     */
    RemoveLastOutput(): void;
    /**
     * Connect an output with the input of another node.
     * @param index Index of the output.
     * @param targetId ID of the target node. Nodes cannot connect to themselves.
     */
    ConnectOutput(index: number, targetId: number): void;
    /**
     * Disconnect an output. Does nothing if output was not connected.
     * @param index Index of output to disconnect.
     */
    DisconnectOutput(index: number): void;
}
