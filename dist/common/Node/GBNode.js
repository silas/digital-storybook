"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBNode = void 0;
const DOMPurify = require("dompurify");
class GBNode {
    constructor() {
        this.attributeCallbacks = [];
        this.attributes = {};
        this.attributeIds = [];
        this.values = {};
        this.templateName = "";
        this.AddAttribute("id", {
            type: "number",
            hidden: true,
            readonly: true
        });
        this.AddAttribute("type", {
            type: "string",
            hidden: true,
            readonly: true
        });
        this.AddAttribute("color", {
            type: "string",
            hidden: true
        }, "slategray");
        this.AddAttribute("textcolor", {
            type: "string",
            hidden: true
        }, "white");
    }
    /* Attribute management */
    /**
     * Defines a new attribute for this node.
     * @param id Unique identifier of the attribute.
     * @param attr Struct describing the attributes properties. See GBNodeAttribute interface.
     * @param value Initial value. If omitted, the value will be based on the attribute type ("" for string, 0 for number and false for boolean).
     */
    AddAttribute(id, attr, value) {
        if (this.HasAttribute(id))
            return;
        this.attributes[id] = attr;
        this.attributeIds.push(id);
        if (value)
            this.SetAttribute(id, value);
        else {
            if (attr.type == "number")
                this.SetAttribute(id, 0);
            else if (attr.type == "string")
                this.SetAttribute(id, "");
            else if (attr.type == "list")
                this.SetAttribute(id, []);
            else if (attr.type == "boolean")
                this.SetAttribute(id, false);
            else if (attr.type == "select")
                this.SetAttribute(id, this.GetAttributeProperty(id, "selectChoices")[0]);
        }
    }
    /**
     * Sets the value of an attribute. Triggers attribute listeners.
     * Does nothing if attribute is not defined.
     * Please note that list attributes are set by reference, not by value.
     * @param id Unique identifier of the attribute.
     * @param value New value of the attribute.
     * @param force If set to true, readonly is ignored.
     */
    SetAttribute(id, value, force = false) {
        if (!this.HasAttribute(id))
            return;
        if (!this.GetAttributeProperty(id, "readonly") || force) {
            let changed = this.values[id] != value;
            this.values[id] = value;
            this.Trigger(id, value, changed);
        }
    }
    /**
     * Gets the value of an attribute. Returns undefined if attribute is not defined.
     * Please note that list attributes return references, not copies.
     * @param id Unique identifier of the attribute.
     * @returns Value of the attribute or undefined.
     */
    GetAttribute(id) {
        if (this.HasAttribute(id)) {
            if (typeof this.values[id] === "string") {
                return DOMPurify.sanitize(this.values[id], {
                    ALLOW_UNKNOWN_PROTOCOLS: true
                });
            }
            else if (Array.isArray(this.values[id]) && this.values[id].length > 0) {
                if (typeof this.values[id][0] === "string") {
                    return this.values[id].map(value => {
                        return DOMPurify.sanitize(value, {
                            ALLOW_UNKNOWN_PROTOCOLS: true
                        });
                    });
                }
                else if (Array.isArray(this.values[id][0])) {
                    return this.values[id].map(val => {
                        if (Array.isArray(val)) {
                            return val.map(subval => {
                                if (typeof subval === "string") {
                                    return DOMPurify.sanitize(subval, {
                                        ALLOW_UNKNOWN_PROTOCOLS: true
                                    });
                                }
                                else {
                                    return subval;
                                }
                            });
                        }
                        else {
                            return val;
                        }
                    });
                }
                else if (typeof this.values[id][0] === "object") {
                    return this.values[id].map(obj => {
                        const res = {};
                        Object.keys(obj).forEach(key => {
                            if (typeof obj[key] === "string") {
                                res[key] = DOMPurify.sanitize(obj[key], {
                                    ALLOW_UNKNOWN_PROTOCOLS: true
                                });
                            }
                            else {
                                res[key] = obj[key];
                            }
                        });
                        return res;
                    });
                }
            }
            return this.values[id];
        }
        return undefined;
    }
    /**
     * @param id Unique identifier of the attribute.
     * @returns True if attribute is defined.
     */
    HasAttribute(id) {
        return this.attributes[id] !== undefined;
    }
    /**
     * Sets an attribute property.
     * @param id Unique identifier of the attribute.
     * @param property Identifier of the property.
     * @param value New value of the property.
     */
    SetAttributeProperty(id, property, value) {
        if (this.HasAttribute(id)) {
            this.attributes[id][property] = value;
        }
    }
    /**
     * @param id Unique identifier of the attribute.
     * @param property Identifier of the property.
     * @returns Value of the property.
     */
    GetAttributeProperty(id, property) {
        if (this.HasAttribute(id)) {
            return this.attributes[id][property];
        }
        return undefined;
    }
    /**
     * @returns The list of attribute ids ordered by their creation.
     */
    get attrIds() {
        return this.attributeIds;
    }
    /* List attribute helpers */
    /**
     * Set the value of a given index for a list attribute.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param index List index to modify.
     * @param value New value.
     * @param force If set to true, readonly is ignored.
     */
    ListAttributeSet(id, index, value, force = false) {
        if (!this.HasAttribute(id))
            return;
        if (!this.GetAttributeProperty(id, "readonly") || force) {
            let list = this.GetAttribute(id);
            if (list && typeof list == "object") {
                list[index] = value;
                this.SetAttribute(id, list);
            }
        }
    }
    /**
     * Get the value of a given index for a list attribute.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param index List index to retrieve.
     * @returns Value of the list at the given index.
     */
    ListAttributeGet(id, index) {
        if (!this.HasAttribute(id))
            return undefined;
        let list = this.GetAttribute(id);
        if (list && typeof list == "object") {
            return list[index];
        }
    }
    /**
     * Pushes a value to the end of a list
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param value Value to push.
     * @param force If set to true, readonly is ignored.
     * @returns The new length of the list or -1 if push failed.
     */
    ListAttributePush(id, value, force = false) {
        if (!this.HasAttribute(id))
            return -1;
        let ret = -1;
        if (!this.GetAttributeProperty(id, "readonly") || force) {
            let list = this.GetAttribute(id);
            if (list && typeof list == "object") {
                ret = list.push(value);
                this.SetAttribute(id, list);
            }
        }
        return ret;
    }
    /**
     * Removes an index from a list attribute.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param index Index to remove.
     */
    ListAttributeRemove(id, index) {
        let list = this.GetAttribute(id);
        if (list && typeof list == "object" && list.length > index) {
            list.splice(index, 1);
            this.SetAttribute(id, list);
        }
    }
    /**
     * Removes and returns the last element in the list.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @returns Value of the removed element.
     */
    ListAttributePop(id) {
        let list = this.GetAttribute(id);
        let ret = undefined;
        if (list && typeof list == "object" && list.length > 0) {
            ret = list.pop();
            this.SetAttribute(id, list);
        }
        return ret;
    }
    /**
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @returns Length of the list attribute.
     */
    ListAttributeLength(id) {
        if (!this.HasAttribute(id))
            return -1;
        let list = this.GetAttribute(id);
        return list.length;
    }
    /* Event management */
    /**
     * Registers an attribute listener.
     * @param id Unique identifier of the attribute.
     * @param callback Callback function executed on attribute change. Parameter is the new value.
     * @param diff If set to true, only triggers if the new value is different from the old one.
     * Does not work with list attributes because they use references which usually stay the same.
     */
    On(id, callback, diff = false) {
        this.attributeCallbacks.push({ id, callback, diff });
    }
    /**
     * Removes an attribute listener.
     * @param id Unique identifier of the attribute.
     * @param callback Registered callback function.
     * @param diff See method GBNode.On(...).
     */
    Off(id, callback, diff = false) {
        this.attributeCallbacks = this.attributeCallbacks.filter(x => {
            return !(x.id == id && x.callback == callback && x.diff == diff);
        });
    }
    /**
     * Trigger attribute listeners. Gets called by attribute setters but can be invoked manually.
     * @param id Unique identifier of the attribute.
     * @param value New value of the attribute.
     * If omitted or undefined, the method automatically gets the current attribute value.
     * @param changed If set to true, listeners with the parameter diff (See method GBNode.On(...)) set to true get triggered as well.
     * True by default.
     */
    Trigger(id, value = undefined, changed = true) {
        if (value === undefined)
            value = this.GetAttribute(id);
        this.attributeCallbacks
            .filter(x => x.id == id)
            .forEach(x => {
            if (!x.diff || (x.diff && changed))
                x.callback(value);
        });
    }
    /* Serialization */
    /**
     * @returns JSON string containing all attribute declarations and values. Can be read with GBNode.Deserialize(...).
     * @see GBNode.Deserialize(...);
     */
    Serialize() {
        return JSON.stringify(this, (key, value) => {
            if (key === "attributeCallbacks")
                return undefined;
            else
                return value;
        });
    }
    /**
     * Parses a JSON string and overwrites all attribute declarations and values.
     * @param json JSON string to parse.
     */
    Deserialize(json) {
        let obj = JSON.parse(json);
        this.attributes = obj.attributes;
        this.attributeIds = obj.attributeIds;
        this.values = obj.values;
        for (let id of this.attributeIds) {
            this.Trigger(id, this.GetAttribute(id), true);
        }
    }
    static ShuffleForEach(arr, callback) {
        let indices = new Array(arr.length);
        for (let i = 0; i < indices.length; ++i) {
            indices[i] = i;
        }
        GBNode.ShuffleArray(indices);
        indices.forEach(index => {
            callback(arr[index], index);
        });
    }
    static ShuffleArray(arr) {
        for (var i = arr.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }
}
exports.GBNode = GBNode;
//# sourceMappingURL=GBNode.js.map