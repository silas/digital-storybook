"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBEndNode = void 0;
const GBLang_1 = require("../../language/GBLang");
const GBEditorNode_1 = require("./GBEditorNode");
class GBEndNode extends GBEditorNode_1.GBEditorNode {
    constructor() {
        super();
        this.AddAttribute("rating", {
            type: "string",
            label: GBLang_1.GBLang.tl("rating", true),
            textarea: true
        });
        this.SetAttribute("title", GBLang_1.GBLang.tl("end", true));
        this.SetAttributeProperty("title", "readonly", true);
        this.SetAttribute("color", "darkgrey");
        this.SetAttribute("textcolor", "black");
    }
}
exports.GBEndNode = GBEndNode;
//# sourceMappingURL=GBEndNode.js.map