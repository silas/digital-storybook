"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBMediaNode = void 0;
// Media nodes
const GBLang_1 = require("../../language/GBLang");
const GBNode_1 = require("./GBNode");
class GBMediaNode extends GBNode_1.GBNode {
    constructor() {
        super();
        this.SetAttribute("type", "image", true);
        this.SetAttribute("color", "orange");
        this.SetAttribute("textcolor", "black");
        this.AddAttribute("tag", {
            type: "string",
            label: GBLang_1.GBLang.tl("mediaTag", true),
            regExp: "^[a-zA-Z_]\\w*$"
        });
        this.AddAttribute("media", {
            type: "select",
            hidden: true,
            selectChoices: ["Image", "Audio", "Video"]
        });
        this.AddAttribute("alt", {
            type: "string",
            label: GBLang_1.GBLang.tl("alt", true),
            textarea: true,
        }, "");
        this.AddAttribute("stretch", {
            type: "boolean",
            label: GBLang_1.GBLang.tl("stretch", true),
        });
        this.AddAttribute("center", {
            type: "boolean",
            label: GBLang_1.GBLang.tl("center", true),
        });
        this.AddAttribute("inline", {
            type: "boolean",
            label: GBLang_1.GBLang.tl("inline", true),
        });
    }
}
exports.GBMediaNode = GBMediaNode;
//# sourceMappingURL=GBMediaNode.js.map