export interface GBNodeAttribute {
    [property: string]: any;
    label?: string;
    type: "number" | "string" | "boolean" | "select" | "list" | "file";
    listType?: "number" | "string" | "boolean" | "select" | "fields";
    hidden?: boolean;
    readonly?: boolean;
    textarea?: boolean;
    selectChoices?: string[];
    regExp?: string;
    name?: string;
    image?: boolean;
    video?: boolean;
    audio?: boolean;
    fields?: {
        label: string;
        textarea?: boolean;
        checkbox?: boolean;
        type?: string;
    }[];
}
export declare abstract class GBNode {
    private attributeCallbacks;
    private attributes;
    private attributeIds;
    private values;
    templateName: string;
    protected constructor();
    /**
     * Defines a new attribute for this node.
     * @param id Unique identifier of the attribute.
     * @param attr Struct describing the attributes properties. See GBNodeAttribute interface.
     * @param value Initial value. If omitted, the value will be based on the attribute type ("" for string, 0 for number and false for boolean).
     */
    AddAttribute(id: string, attr: GBNodeAttribute, value?: any): void;
    /**
     * Sets the value of an attribute. Triggers attribute listeners.
     * Does nothing if attribute is not defined.
     * Please note that list attributes are set by reference, not by value.
     * @param id Unique identifier of the attribute.
     * @param value New value of the attribute.
     * @param force If set to true, readonly is ignored.
     */
    SetAttribute(id: string, value: any, force?: boolean): void;
    /**
     * Gets the value of an attribute. Returns undefined if attribute is not defined.
     * Please note that list attributes return references, not copies.
     * @param id Unique identifier of the attribute.
     * @returns Value of the attribute or undefined.
     */
    GetAttribute(id: string): any;
    /**
     * @param id Unique identifier of the attribute.
     * @returns True if attribute is defined.
     */
    HasAttribute(id: string): boolean;
    /**
     * Sets an attribute property.
     * @param id Unique identifier of the attribute.
     * @param property Identifier of the property.
     * @param value New value of the property.
     */
    SetAttributeProperty(id: string, property: string, value: any): void;
    /**
     * @param id Unique identifier of the attribute.
     * @param property Identifier of the property.
     * @returns Value of the property.
     */
    GetAttributeProperty(id: string, property: string): any;
    /**
     * @returns The list of attribute ids ordered by their creation.
     */
    get attrIds(): string[];
    /**
     * Set the value of a given index for a list attribute.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param index List index to modify.
     * @param value New value.
     * @param force If set to true, readonly is ignored.
     */
    ListAttributeSet(id: string, index: number, value: any, force?: boolean): void;
    /**
     * Get the value of a given index for a list attribute.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param index List index to retrieve.
     * @returns Value of the list at the given index.
     */
    ListAttributeGet(id: string, index: number): any;
    /**
     * Pushes a value to the end of a list
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param value Value to push.
     * @param force If set to true, readonly is ignored.
     * @returns The new length of the list or -1 if push failed.
     */
    ListAttributePush(id: string, value: any, force?: boolean): number;
    /**
     * Removes an index from a list attribute.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @param index Index to remove.
     */
    ListAttributeRemove(id: string, index: number): void;
    /**
     * Removes and returns the last element in the list.
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @returns Value of the removed element.
     */
    ListAttributePop(id: string): any;
    /**
     * Does nothing if attribute is not a list.
     * @param id Unique identifier of the attribute.
     * @returns Length of the list attribute.
     */
    ListAttributeLength(id: string): number;
    /**
     * Registers an attribute listener.
     * @param id Unique identifier of the attribute.
     * @param callback Callback function executed on attribute change. Parameter is the new value.
     * @param diff If set to true, only triggers if the new value is different from the old one.
     * Does not work with list attributes because they use references which usually stay the same.
     */
    On(id: string, callback: {
        (value?: any): void;
    }, diff?: boolean): void;
    /**
     * Removes an attribute listener.
     * @param id Unique identifier of the attribute.
     * @param callback Registered callback function.
     * @param diff See method GBNode.On(...).
     */
    Off(id: string, callback: {
        (value?: any): void;
    }, diff?: boolean): void;
    /**
     * Trigger attribute listeners. Gets called by attribute setters but can be invoked manually.
     * @param id Unique identifier of the attribute.
     * @param value New value of the attribute.
     * If omitted or undefined, the method automatically gets the current attribute value.
     * @param changed If set to true, listeners with the parameter diff (See method GBNode.On(...)) set to true get triggered as well.
     * True by default.
     */
    Trigger(id: string, value?: any, changed?: boolean): void;
    /**
     * @returns JSON string containing all attribute declarations and values. Can be read with GBNode.Deserialize(...).
     * @see GBNode.Deserialize(...);
     */
    Serialize(): string;
    /**
     * Parses a JSON string and overwrites all attribute declarations and values.
     * @param json JSON string to parse.
     */
    Deserialize(json: string): void;
    static ShuffleForEach<T>(arr: T[], callback: (element: T, index: number) => void): void;
    static ShuffleArray(arr: any[]): void;
}
