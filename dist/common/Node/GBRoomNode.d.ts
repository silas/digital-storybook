import { GBEditorNode } from "./GBEditorNode";
export declare abstract class GBRoomNode extends GBEditorNode {
    constructor();
    /**
     * Should render the node the renderTarget.
     * There should be rendered elements which call the select function with valid outputs.
     * @param select selects output for further execution. Should be called by DOM listeners.
     * @param scope Variable register.
     * @param renderTarget HTMLElement to render to.
     */
    abstract Render(select: {
        (output: number): void;
    }, scope: {
        [key: string]: any;
    }, renderTarget: HTMLElement): void;
}
