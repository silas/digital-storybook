import { GBMenuItem } from "./GBMenuItem";
export declare class GBMenu {
    private _container;
    private menuContainer;
    private menu;
    private id;
    private _open;
    private menuItems;
    private subMenus;
    constructor(container: HTMLElement, id?: string);
    /**
     * Add a new menu item
     * @param text Label of item
     * @param callback Method to call on click
     * @returns Menu item
     */
    AddMenuItem(text?: string, callback?: {
        (event: MouseEvent): void;
    }): GBMenuItem;
    /**
     * Remove a menu item
     * @param item item to remove
     */
    RemoveMenuItem(item: GBMenuItem): void;
    /**
     * Get an array containing all menu items
     * @returns Array containing all menu items
     */
    GetMenuItems(): GBMenuItem[];
    /**
     * Add a new submenu
     * @param text Label for the submenu
     * @param id
     * @returns new submenu
     */
    AddSubMenu(text: string, id?: string): GBMenu;
    /**
     * Remove a submenu
     * @param menu submenu to remove
     */
    RemoveSubMenu(menu: GBMenu): void;
    /**
     * Get an array containing all sub menus
     * @returns Array containing all sub menus
     */
    GetSubMenus(): GBMenu[];
    /**
     * Open the menu
     */
    Open(): void;
    /**
     * Close the menu
     */
    Close(): void;
    /**
     * Toggle the menu
     */
    Toggle(): void;
    get open(): boolean;
    get container(): HTMLElement;
}
