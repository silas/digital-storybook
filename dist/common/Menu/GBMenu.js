"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBMenu = void 0;
const GBMenuItem_1 = require("./GBMenuItem");
class GBMenu {
    constructor(container, id) {
        this.id = "";
        this._open = true;
        this.menuItems = [];
        this.subMenus = [];
        this._container = container;
        this.id = id;
        this.menuContainer = document.createElement("div");
        this.menu = document.createElement("ul");
        this.menuContainer.appendChild(this.menu);
        this._container.appendChild(this.menuContainer);
    }
    /**
     * Add a new menu item
     * @param text Label of item
     * @param callback Method to call on click
     * @returns Menu item
     */
    AddMenuItem(text = "", callback) {
        let item = new GBMenuItem_1.GBMenuItem(this.menu, text, callback);
        this.menuItems.push(item);
        return item;
    }
    /**
     * Remove a menu item
     * @param item item to remove
     */
    RemoveMenuItem(item) {
        item.Remove();
        this.menuItems = this.menuItems.filter(i => i !== item);
    }
    /**
     * Get an array containing all menu items
     * @returns Array containing all menu items
     */
    GetMenuItems() {
        return this.menuItems;
    }
    /**
     * Add a new submenu
     * @param text Label for the submenu
     * @param id
     * @returns new submenu
     */
    AddSubMenu(text, id) {
        let subcontainer = document.createElement("div");
        new GBMenuItem_1.GBMenuItem(subcontainer, text + " >", () => {
            submenu.Toggle();
        });
        let submenu = new GBMenu(subcontainer, id);
        submenu.Close();
        this.menu.appendChild(subcontainer);
        this.subMenus.push(submenu);
        return submenu;
    }
    /**
     * Remove a submenu
     * @param menu submenu to remove
     */
    RemoveSubMenu(menu) {
        this.menu.removeChild(menu.container);
        this.subMenus = this.subMenus.filter(m => m !== menu);
    }
    /**
     * Get an array containing all sub menus
     * @returns Array containing all sub menus
     */
    GetSubMenus() {
        return this.subMenus;
    }
    /**
     * Open the menu
     */
    Open() {
        this.menuContainer.setAttribute("class", "");
        this._open = true;
    }
    /**
     * Close the menu
     */
    Close() {
        this.menuContainer.setAttribute("class", "hidden");
        this._open = false;
    }
    /**
     * Toggle the menu
     */
    Toggle() {
        if (this.open)
            this.Close();
        else
            this.Open();
    }
    /// Accessors
    get open() {
        return this._open;
    }
    get container() {
        return this._container;
    }
}
exports.GBMenu = GBMenu;
//# sourceMappingURL=GBMenu.js.map