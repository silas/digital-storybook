"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBMenubar = void 0;
const GBMenu_1 = require("./GBMenu");
class GBMenubar {
    constructor(container, style = "") {
        this._open = false;
        this.style = style;
        this.menubar = document.createElement("div");
        this.menubar.setAttribute("class", this.style);
        this._menu = new GBMenu_1.GBMenu(this.menubar);
        container.appendChild(this.menubar);
    }
    Open() {
        this.menubar.setAttribute("class", this.style + " open");
        this._open = true;
    }
    Close() {
        this.menubar.setAttribute("class", this.style);
        this._open = false;
    }
    Toggle() {
        if (this.open)
            this.Close();
        else
            this.Open();
    }
    get open() {
        return this._open;
    }
    get menu() {
        return this._menu;
    }
}
exports.GBMenubar = GBMenubar;
//# sourceMappingURL=GBMenubar.js.map