"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GBMenuItem = void 0;
class GBMenuItem {
    constructor(container, text = "", callback) {
        this.callback = function () { };
        this.container = container;
        this.item = document.createElement("li");
        this.anchor = document.createElement("a");
        this.anchor.setAttribute("tabindex", "0");
        this.text = text;
        if (callback)
            this.SetCallback(callback);
        this.item.appendChild(this.anchor);
        this.container.appendChild(this.item);
    }
    /**
     * Set the callback method of the menu item
     * @param callback Callback method
     */
    SetCallback(callback) {
        this.anchor.removeEventListener("click", this.callback);
        this.callback = (e) => {
            e.preventDefault();
            e.stopPropagation();
            callback(e);
        };
        this.anchor.addEventListener("click", this.callback);
    }
    /**
     * Remove menu item from menu
     */
    Remove() {
        this.container.removeChild(this.item);
    }
    /// Accessors
    set text(text) {
        this.anchor.textContent = text;
    }
    get text() {
        return this.anchor.textContent ? this.anchor.textContent : "";
    }
}
exports.GBMenuItem = GBMenuItem;
//# sourceMappingURL=GBMenuItem.js.map