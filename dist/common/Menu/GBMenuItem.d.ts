export declare class GBMenuItem {
    private container;
    private callback;
    private item;
    private anchor;
    constructor(container: HTMLElement, text?: string, callback?: {
        (event: MouseEvent): void;
    });
    /**
     * Set the callback method of the menu item
     * @param callback Callback method
     */
    SetCallback(callback: {
        (e: MouseEvent): void;
    }): void;
    /**
     * Remove menu item from menu
     */
    Remove(): void;
    set text(text: string);
    get text(): string;
}
