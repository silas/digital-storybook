import { GBMenu } from "./GBMenu";
export declare class GBMenubar {
    private style;
    private menubar;
    private _menu;
    private _open;
    constructor(container: HTMLElement, style?: string);
    Open(): void;
    Close(): void;
    Toggle(): void;
    get open(): boolean;
    get menu(): GBMenu;
}
